# Code used in my bachelor thesis
Singlematerial folder includes all tests needed for singlematerial simulation.
Multimaterial for multimaterial one.

What each file does in each folder is similar if not same. 

## Included files
Ordered from the most to least interesting

| File              | Description                                                                                                      |
|-------------------|------------------------------------------------------------------------------------------------------------------|
| settings.py       | Module containing most of the settings                                                                           |
| test_functions.py | Module which includes functions used for testing                                                                 |
| main.py           |                                                                                                                  |
| mesh.py           | Module containing the Mesh class as well as all related functions (remapping, reconstruction, calculating error) |
| integrate.py      | Module containing all integration methods                                                                        |
| writer.py         | Module containing functions to easily output results                                                             |
| drawing.py        | Module containing functions to draw graphs                                                                       |
| cell.py           | Class for cells                                                                                                  |
| node.py           | Class for nodes + functions for creating grids                                                                   |
| notebook.ipynb    | Mostly used for testing, might get removed                                                                       |
| temp_results.txt  | Temporary way of writing down intermediate results, will be removed in future                                    |