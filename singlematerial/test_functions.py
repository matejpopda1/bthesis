import numpy as np


# Definitions of decorators
def list_of_discontinuities(discontinuities):
    """
    Adds a list of discontinuities to the function, these are used for THINC reconstruction
    """
    if not isinstance(discontinuities, list):
        raise RuntimeError("Provided discontinuities are not a list")

    def wrapper(function):
        function.list_of_discontinuities = discontinuities
        return function
    return wrapper


# Definitions of decorators
def list_of_limited_cells(limited_cells):
    """
    Adds positions of nodes where to do limit
    """
    if not isinstance(limited_cells, list):
        raise RuntimeError("Provided discontinuities are not a list")

    def wrapper(function):
        function.list_of_limited_cells = limited_cells
        return function
    return wrapper


def linear(x):
    """
    f(x) = x
    """
    return x


@list_of_discontinuities([0])
def jump(x):
    """
    f(x) = Heavyside(x - 0.5) + 1
    """
    if x > 0:
        return 2
    return 1


@list_of_discontinuities([3, 4])
def discontinuous(x):
    """
    f(x) = Heavyside(-x + 3)*sin_2nd_degree(x) + Heavyside(x - 4)*cos(x)
    """
    if x < 3:
        return np.sin(x)
    if x > 4:
        return np.cos(x)
    return 0


def constant(x):
    """
    f(x) = 1
    """
    return 1


def sine(x):
    """
    f(x) = sin_2nd_degree(x)
    """
    return np.sin(x)


def oscillating(x):
    """
    f(x) = sin_2nd_degree(10x) + sin_2nd_degree(100/3 x)
    """
    x = x*10
    return np.sin(x) + np.sin((10.0 / 3.0) * x)


def quadratic(x):
    """
    f(x) = x^2
    """
    return x*x


# Function from Second-Order Sign-Preserving Remapping on General Grids
def test_function(x):
    """
    f(x) = 1 + sin_2nd_degree(2*pi*x)
    """
    return 1 + np.sin(np.pi * 2 * x)


def septic(x):
    """
    f(x) = x^7
    """
    return x**7

@list_of_discontinuities([0])
def lin_lin_jump(x):
    """
    f(x) = Heavyside(x - 0.5) + 1
    """
    if x > 0:
        return - 3 - 3*x
    return 2 - 2*x


@list_of_discontinuities([0.00001])
def quad_quad_interface(x):
    if x > 0:
        return x * x + 1 - x
    else:
        return - x * x + 2 - x


@list_of_limited_cells([0, 0.1, 0.2, 0.4, 0.6])
@list_of_discontinuities([-0.4, -0.2])
def four_shape_profile(x):
    """
    function from 'High-accurate and robust ...'
    """
    result = 1
    a = 0.5
    z = -0.7
    delta = 0.005
    alpha = 10
    beta = np.log(2) / (36 * delta**2)

    def F(x_, alpha_, a_):
        return np.sqrt(max(1 - (alpha_**2) * (x_ - a_)**2, 0))

    def G(x_, beta_, z_):
        return np.exp(-beta_*((x_ - z_)**2))

    if -0.8 < x < -0.6:
        result += 1 + (G(x, beta, z - delta) + G(x, beta, z + delta) + 4*G(x, beta, z))/6
    elif -0.4 < x < -0.2:
        result += 2
    elif 0 < x < 0.2:
        result += 2 - abs(10*(x - 0.1))
    elif 0.4 < x < 0.6:
        result += 1 + (F(x, alpha, a - delta) + F(x, alpha, a + delta) + 4 * F(x, alpha, a))/6
    else:
        result += 1
    return result


def maple(x):
    return 4.612246667 + 13.12500000*x - 55.*x**2 + 85.*x**3 - 54.*x**4 + 10.*x**5
