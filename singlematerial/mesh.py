from typing import List

import numpy
import numpy as np
import scipy.linalg

import settings
import node
import cell
import integrate


class Mesh:
    def __init__(self):
        self.nodes: List[node.Node] = []  # a list of nodes
        self.cells: List[cell.Cell] = []  # a list of cells
        self.ghost_cells: List[cell.Cell] = []  # a list of cells outside computational domain
        self.ghost_nodes: List[
            node.Node] = []  # a list of nodes outside the computational domain, first there are right nodes, then left

    def __del__(self):
        try:
            integral_after_remap = integrate.integrate_mesh(self, settings.starting_point, settings.ending_point)
            integral_original_function = integrate.scipy_integration(settings.input_function, settings.starting_point,
                                                                     settings.ending_point)
            print("Deleting mesh, difference of the starting - ending value is " + str(
                integral_original_function - integral_after_remap) + "\nWhere the original value is " + str(
                integral_original_function) + " and the new value is " + str(integral_after_remap))
        except KeyError:  # this error should be raised because this is a destructor, ie on cleanup we are running this piece of code
            pass

    def inside_what_cell(self, x: float):
        """
        Binary search, otherwise too slow
        :return: index of the cell x is in
        """
        left = 0
        right = len(self.nodes) - 1
        while left <= right:
            mid = (right + left) // 2
            # If x is greater, ignore left half
            if self.nodes[mid + 1].position < x:
                left = mid + 1
            # If x is smaller, ignore right half
            elif self.nodes[mid].position > x:
                right = mid - 1
            # means x is present at mid
            else:
                return mid
        raise RuntimeWarning(
            "Exception " + str(x) + " is outside of domain")  # could add handling but it shouldn't be needed

    def value_at_x(self, x: float):
        """
        Returns the value at x, Reconstruction must be called before!!!
        :param x: number from <settings.starting_point,settings.ending_point>
        :return: the value of the reconstruction at x
        """
        if x < settings.starting_point or x > settings.ending_point:
            raise RuntimeError("Outside of computational domain: " + str(x))
        index_of_current_cell = self.inside_what_cell(x)
        current_cell = self.cells[index_of_current_cell]
        if current_cell.reconstruction_type[0][0] == "P":
            result = 0
            h = x - current_cell.mid_point
            for i, derivative in enumerate([current_cell.value] + current_cell.derivative):
                result += (pow(h, i) - current_cell.conservation_constants[i]) * derivative
        elif current_cell.reconstruction_type[0] == "THINC":
            parameters = current_cell.params_for_other_reconstructions
            x_left_boundary = self.nodes[index_of_current_cell].position
            x_right_boundary = self.nodes[index_of_current_cell + 1].position

            argument_of_tanh = parameters["beta"] * (
                        ((x - x_left_boundary) / (x_right_boundary - x_left_boundary)) - parameters[
                    "hyperbolic_conservation_constant"])
            result = parameters["a_min"]
            result += (parameters["a_max"] - parameters["a_min"]) / 2 * (
                        1 + parameters["theta"] * numpy.tanh(argument_of_tanh))
        elif current_cell.reconstruction_type[0] == "JUMP":
            parameters = current_cell.params_for_other_reconstructions
            # if x < parameters["jump_position"]:
            #     return parameters["left_val"]
            # else:
            #     return parameters["right_val"]

            return parameters["fmin"] + parameters["fdif"] * numpy.heaviside(
                parameters["theta"] * (x - parameters["hatx"]), 0)

        else:
            raise NotImplementedError(current_cell.reconstruction_type[0] + "reconstruction isnt implemented")
        return result

    def remap(self, list_of_new_nodes: List[node.Node]):
        """
        :param list_of_new_nodes: ordered list of new nodes, first node must be at x = 0, last node must be at x = 1
        """
        temp_cells: List[cell.Cell] = []
        # creating cells
        for i in range(len(list_of_new_nodes) - 1):
            volume = list_of_new_nodes[i + 1].position - list_of_new_nodes[i].position
            mid_point = (list_of_new_nodes[i].position + list_of_new_nodes[i + 1].position) / 2
            temp_cell = cell.Cell(volume, mid_point)
            temp_cells.append(temp_cell)
        self.reconstruction()
        # integrating over cells
        for i in range(len(temp_cells)):
            temp_cells[i].value = integrate.integrate_mesh(self, list_of_new_nodes[i].position, list_of_new_nodes[
                i + 1].position)  # this has to be divided by volume
        for i in temp_cells:
            i.value /= i.volume
            i.reconstruction_type = settings.global_reconstruction_type
        self.nodes = list_of_new_nodes
        self.cells = temp_cells
        self.designate_cells_for_non_global_reconstruction()

    def designate_cells_for_non_global_reconstruction(self):
        if hasattr(settings.input_function, 'list_of_limited_cells') and settings.use_limiter_at_selected_cells is True:
            for limiters in settings.input_function.list_of_limited_cells:
                index_of_cell = self.inside_what_cell(limiters)
                self.cells[index_of_cell].reconstruction_type = ["P+BJ", 1]

        if settings.do_non_polynomial_reconstruction is True and hasattr(settings.input_function,
                                                                         'list_of_discontinuities'):
            for discontinuity in settings.input_function.list_of_discontinuities:
                index_of_cell = self.inside_what_cell(discontinuity)
                self.cells[index_of_cell].reconstruction_type = settings.non_polynomial_reconstruction_type
                how_many_neighbour_cells_to_change = np.int_(np.ceil(settings.max_polynomial_degree / 2))
                for neighbour_cells_relative_index in range(- how_many_neighbour_cells_to_change,
                                                            how_many_neighbour_cells_to_change + 1):
                    if neighbour_cells_relative_index != 0:
                        self.cells[
                            index_of_cell + neighbour_cells_relative_index].reconstruction_type = settings.reconstruction_type_in_cells_next_to_a_discontinuity

    def reconstruction(self):
        """
        Handles calculating all the parameters for reconstruction, ie. derivatives and parameters for THINC reconstruction
        """
        possible_reconstruction_types = ["P", "P+BJ", "THINC", "JUMP"]

        joined_cells = self.cells + self.ghost_cells  # only used for the polynomial reconstruction

        for i in range(0, len(self.cells)):
            if self.cells[i].reconstruction_type[0] not in possible_reconstruction_types:
                raise NotImplementedError(
                    self.cells[i].reconstruction_type[0] + " is not implemented reconstruction type")

            if self.cells[i].reconstruction_type[0][
                0] == "P":  # checks if the first character of the string is "P" implying polynomial reconstruction TODO: Consider moving this into one function
                self.cells[i].derivative = [0]
                if self.cells[i].reconstruction_type[1] == 0:
                    pass
                elif self.cells[i].reconstruction_type[1] == 1:
                    self.cells[i].derivative[0] = (joined_cells[i + 1].value - joined_cells[i - 1].value) / (
                                joined_cells[i + 1].mid_point - joined_cells[i - 1].mid_point)
                elif self.cells[i].reconstruction_type[1] > 1:
                    self.cells[i].derivative = self.calculate_higher_order_derivatives_in_a_cell(i, joined_cells)
                else:  # TODO: handling for non-whole number inputs?
                    raise ValueError("Degree of polynomial in reconstruction must be non-negative, degree given is" +
                                     self.cells[i].reconstruction_type[1])

        for i in range(0, len(self.cells)):
            if self.cells[i].reconstruction_type[0] == "P+BJ":
                if self.cells[i].reconstruction_type[1] > 1:
                    raise NotImplementedError("BJ limiter is only implemented for maximum degree of polynomial of 1 ")
                local_maximum = max(joined_cells[i].value, joined_cells[i + 1].value, joined_cells[i - 1].value)
                local_minimum = min(joined_cells[i].value, joined_cells[i + 1].value, joined_cells[i - 1].value)
                phi = 1

                def first_order_tailor(x):
                    return joined_cells[i].value + joined_cells[i].derivative[0] * (x - joined_cells[i].mid_point)

                for j in [0, 1]:
                    denominator = first_order_tailor(self.nodes[i + j].position) - joined_cells[i].value
                    if denominator > 0:
                        phi = min(phi, (local_maximum - joined_cells[i].value) / denominator)
                    elif denominator < 0:
                        phi = min(phi, (local_minimum - joined_cells[i].value) / denominator)

                self.cells[i].derivative[0] = phi * self.cells[i].derivative[0]

        for i in range(0, len(self.cells)):
            if self.cells[i].reconstruction_type[0] == "THINC":
                self.cells[i].params_for_other_reconstructions["theta"] = np.sign(self.cells[i + 1].value - self.cells[i - 1].value)
                self.cells[i].params_for_other_reconstructions["beta"] = settings.THINC_beta

                def reconstruction_at_k_cell_at_x_linear(k, x):
                    return self.cells[k].value + self.cells[k].derivative[0] * (x - self.cells[k].mid_point)

                value_at_left_boundary = reconstruction_at_k_cell_at_x_linear(i - 1, self.nodes[i].position)
                value_at_right_boundary = reconstruction_at_k_cell_at_x_linear(i + 1, self.nodes[i + 1].position)

                self.cells[i].params_for_other_reconstructions["a_min"] = min(value_at_left_boundary,
                                                                              value_at_right_boundary)
                self.cells[i].params_for_other_reconstructions["a_max"] = max(value_at_left_boundary,
                                                                              value_at_right_boundary)

                a_min = self.cells[i].params_for_other_reconstructions["a_min"]
                a_max = self.cells[i].params_for_other_reconstructions["a_max"]

                # these values could be omitted but more readable this way
                beta = self.cells[i].params_for_other_reconstructions["beta"]
                theta = self.cells[i].params_for_other_reconstructions["theta"]

                # rescaling of value to interval of 0 to 1 (as in the paper)
                normalized_value = (self.cells[i].value - a_min) / (a_max - a_min)

                # numerator = np.exp((beta/theta) * (1 + theta - 2 * normalized_value))
                # denominator = (1 - (np.exp((beta/theta) * (1 - theta - 2 * normalized_value))))

                # inside_of_log = numerator/denominator
                #
                # self.cells[i].params_for_other_reconstructions["hyperbolic_conservation_constant"] = 1/(2 * beta) * np.log(inside_of_log)
                epstanh = 10**-20
                epsatanh = 10**-8

                B = np.exp(theta * beta * (2 * (self.cells[i].value - a_min + epstanh) / (a_max - a_min + epstanh) - 1))
                A = (B / np.cosh(beta) - 1) / np.tanh(beta)

                if A > 1:
                    A = 1 - epsatanh
                elif A < -1:
                    A = -1 + epsatanh

                self.cells[i].params_for_other_reconstructions["hyperbolic_conservation_constant"] = - 1 / beta * np.arctanh(A)

                left_node = self.nodes[i].position
                right_node = self.nodes[i + 1].position
                selfval = self.cells[i].value
                # print("")
                # print(integrate.integrate_mesh(self, left_node, right_node) / (right_node - left_node) - selfval)
                # print(integrate.integrate_mesh(self, left_node, right_node) / (right_node - left_node))
                # print(selfval)


            # elif self.cells[i].reconstruction_type[0] == "JUMP":
            #     left_val = self.cells[i-1].value
            #     right_val = self.cells[i+1].value
            #     left_node = self.nodes[i].position
            #     right_node = self.nodes[i+1].position
            #
            #     jump_position = (self.cells[i].value * (right_node - left_node) - right_node * right_val + left_val * left_node)/(left_val - right_val)
            #
            #     self.cells[i].params_for_other_reconstructions["left_val"] = left_val
            #     self.cells[i].params_for_other_reconstructions["right_val"] = right_val
            #     self.cells[i].params_for_other_reconstructions["jump_position"] = jump_position
            elif self.cells[i].reconstruction_type[0] == "JUMP":
                left_val = self.cells[i - 1].value
                right_val = self.cells[i + 1].value
                left_node = self.nodes[i].position
                right_node = self.nodes[i + 1].position

                def reconstruction_at_k_cell_at_x_linear(k, x):
                    return self.cells[k].value + self.cells[k].derivative[0] * (x - self.cells[k].mid_point)

                value_at_left_boundary = reconstruction_at_k_cell_at_x_linear(i - 1, self.nodes[i].position)
                value_at_right_boundary = reconstruction_at_k_cell_at_x_linear(i + 1, self.nodes[i + 1].position)

                self.cells[i].params_for_other_reconstructions["a_min"] = min(value_at_left_boundary,
                                                                              value_at_right_boundary)
                self.cells[i].params_for_other_reconstructions["a_max"] = max(value_at_left_boundary,
                                                                              value_at_right_boundary)

                fmin = self.cells[i].params_for_other_reconstructions["a_min"]
                fmax = self.cells[i].params_for_other_reconstructions["a_max"]
                selfval = self.cells[i].value
                fdif = fmax - fmin


                theta = numpy.sign(right_val - left_val)
                hatx = -theta * ((right_node - left_node) * (selfval - fmin) / fdif)

                if theta > 0:
                    hatx += right_node
                else:
                    hatx += left_node

                self.cells[i].params_for_other_reconstructions["theta"] = theta
                self.cells[i].params_for_other_reconstructions["fmin"] = fmin
                self.cells[i].params_for_other_reconstructions["fdif"] = fdif
                self.cells[i].params_for_other_reconstructions["hatx"] = hatx

                if abs(integrate.integrate_mesh(self, left_node, right_node) / (right_node - left_node) - selfval) > 10**-6:
                    print("here")
                    self.cells[i].reconstruction_type = ["P", 0]

                # print("")
                # print(integrate.integrate_mesh(self, left_node, right_node) / (right_node - left_node) - selfval)
                # print(integrate.integrate_mesh(self, left_node, right_node) / (right_node - left_node))
                # print(selfval)


            elif self.cells[i].reconstruction_type[0] == "POLYJUMP":
                raise NotImplementedError("Polyjump not fully implemented yet")

                left_val = self.cells[i - 1].value
                right_val = self.cells[i + 1].value
                left_node = self.nodes[i].position
                right_node = self.nodes[i + 1].position

                # def find_interface():
                #     interface = left_node
                #     while integrate.integrate_mesh(self, )

                # jump_position = find_interface()

                self.cells[i].params_for_other_reconstructions["left_val"] = left_val
                self.cells[i].params_for_other_reconstructions["right_val"] = right_val
                # self.cells[i].params_for_other_reconstructions["jump_position"] = jump_position

        if settings.do_tests is True:
            for i in range(0, len(self.cells)):
                if self.cells[i].conservation_constants[0] != 0 or self.cells[i].conservation_constants[1] != 0:
                    print("Error conservation constant is not a 0, it is " + str(
                        self.cells[i].conservation_constants[1]) + " and " + str(
                        self.cells[i].conservation_constants[0]) + " instead")
                if self.cells[i].reconstruction_type[0][0] == "P" and self.cells[i].reconstruction_type[1] != 0:
                    if len(self.cells[i].conservation_constants) != self.cells[i].reconstruction_type[1] + 1:
                        print("Error conservation constant is not equal to the reconstruction degree, it is " + str(
                            len(self.cells[i].conservation_constants)) + " instead")
                if self.cells[i].reconstruction_type[0] == "THINC":
                    # check if mean values of cells i-1, i, i+1 form a strictly increasing/decreasing sequence
                    if not ((self.cells[i - 1].value < self.cells[i].value < self.cells[i + 1].value) or (
                            self.cells[i - 1].value > self.cells[i].value > self.cells[i + 1].value)):
                        raise RuntimeError(
                            "Values around provided discontinuity in cell " + str() + " do not for a strictly increasing/decreasing sequence ")

            if self.nodes[0].position < settings.starting_point:
                print("Error first node is out of computational domain " + str(self.nodes[0].position))
            if self.nodes[len(self.cells)].position < settings.starting_point:
                print("Error last node is out of computational domain " + str(self.nodes[len(self.cells)].position))

    def calculate_higher_order_derivatives_in_a_cell(self, index_of_current_cell: int, joined_cells):
        """
        Outputs the derivatives and changes conservation constants
        """

        degree_of_reconstruction = self.cells[index_of_current_cell].reconstruction_type[1]
        derivatives = np.zeros(degree_of_reconstruction)  # S_i
        conservation_constants = np.zeros(degree_of_reconstruction + 1)  # K_i
        joined_nodes = self.nodes + self.ghost_nodes

        for i in range(2,
                       degree_of_reconstruction + 1):  # calculates the conservation constants, ignoring the first 2 since its analytically 0
            def f(x):  # could be rewritten with lambda function
                return pow((x - self.cells[index_of_current_cell].mid_point), i)

            conservation_constants[i] = integrate.scipy_integration(f, self.nodes[index_of_current_cell].position,
                                                                    self.nodes[index_of_current_cell + 1].position) / \
                                        self.cells[index_of_current_cell].volume
        self.cells[index_of_current_cell].conservation_constants = conservation_constants.tolist()

        # calculate how many neighbours are needed
        number_of_neighbours = degree_of_reconstruction
        if number_of_neighbours % 2 != 0:
            number_of_neighbours += 1

        # creating the system
        a = np.zeros((number_of_neighbours, degree_of_reconstruction))
        b = np.zeros(number_of_neighbours)
        nn = - 1
        for neighbour_cells_index in range(index_of_current_cell - number_of_neighbours // 2,
                                           index_of_current_cell + number_of_neighbours // 2 + 1):
            if neighbour_cells_index == index_of_current_cell:
                pass
            else:
                nn += 1
                for slopes in range(1, degree_of_reconstruction + 1):
                    def f(x):
                        return (x - self.cells[index_of_current_cell].mid_point) ** slopes - conservation_constants[
                            slopes]

                    temp = integrate.scipy_integration(f, joined_nodes[neighbour_cells_index].position,
                                                       joined_nodes[neighbour_cells_index + 1].position)
                    a[nn, slopes - 1] = temp / self.cells[index_of_current_cell].volume ** (slopes - 1) / joined_cells[
                        neighbour_cells_index].volume
                b[nn] = joined_cells[neighbour_cells_index].value - joined_cells[index_of_current_cell].value

        # calculating pseudoinverse
        psinv = scipy.linalg.pinv(a)

        # calculating result
        result_of_linalg = np.matmul(psinv, b)

        # rescaling
        for i in range(degree_of_reconstruction):
            derivatives[i] = result_of_linalg[i] / self.cells[index_of_current_cell].volume ** i
        return derivatives.tolist()

    def l1_error(self, function):
        """
        Turns function into mesh with the same nodes as the current version of the mesh, then calculates the error
        :param function: original analytical function
        :return: L1 error of the current mesh
        """
        original_mesh = function_to_mesh_from_node_list(function, self.nodes)

        numerator = 0
        denominator = 0
        for i in range(len(self.cells)):
            numerator += abs(self.cells[i].value - original_mesh.cells[i].value) * self.cells[i].volume
            denominator += abs(original_mesh.cells[i].value) * self.cells[i].volume
        return numerator / denominator

    def l2_error(self, function):
        """
        :param function: original analytical function
        :return: L2 error of the current mesh
        """
        original_mesh = function_to_mesh_from_node_list(function, self.nodes)

        numerator = 0
        denominator = 0
        for i in range(len(self.cells)):
            numerator += pow(self.cells[i].value - original_mesh.cells[i].value, 2) * self.cells[i].volume
            denominator += pow(original_mesh.cells[i].value, 2) * self.cells[i].volume

        print("{0:.15f}".format(numerator/denominator))
        return numerator / denominator

    def l_inf_error(self, function):
        """
        :param function: original analytical function
        :return: L_inf error of the current mesh
        """
        original_mesh = function_to_mesh_from_node_list(function, self.nodes)

        numerator = 0
        denominator = 0
        for i in range(len(self.cells)):
            numerator = max(abs(self.cells[i].value - original_mesh.cells[i].value) * self.cells[i].volume, numerator)
            denominator = max(original_mesh.cells[i].value * self.cells[i].volume, denominator)
        return numerator / denominator

    def max_error(self, function):
        """
        :param function: original analytical function
        :return: max error of the current mesh
        """
        original_mesh = function_to_mesh_from_node_list(function, self.nodes)

        result = 0
        for i in range(len(self.cells)):
            result = max(self.cells[i].value - original_mesh.cells[i].value, result)
        return result


def function_to_mesh_from_node_list(function, list_of_nodes: List) -> "Mesh":
    """
    Takes a function from interval <starting_point,ending_point> and turns it into a mesh
    :param list_of_nodes:
    :param function: function that is getting turned into a mesh
    :return: resulting mesh
    """
    mesh = Mesh()

    # creating nodes
    mesh.nodes = list_of_nodes

    # creating cells
    for i in range(len(list_of_nodes) - 1):
        volume = mesh.nodes[i + 1].position - mesh.nodes[i].position
        midpoint = (mesh.nodes[i].position + mesh.nodes[i + 1].position) / 2
        temp_cell = cell.Cell(volume, midpoint, integrate.scipy_integration(function, mesh.nodes[i].position,
                                                                            mesh.nodes[i + 1].position) / volume)
        temp_cell.reconstruction_type = settings.global_reconstruction_type  # TODO: replace hard coded value with some sort of detector?
        mesh.cells.append(temp_cell)

    # creating ghost cells
    # creating temp nodes  TODO: get rid of duplicated code?
    temp_ghost_nodes_l = node.create_equidistant_nodes((settings.max_polynomial_degree // 2) + 2,
                                                       mesh.nodes[0].position - (
                                                               settings.ending_point - settings.starting_point) * (
                                                               settings.overshoot_as_percentage_of_total_computational_domain / 100),
                                                       mesh.nodes[0].position)
    temp_ghost_nodes_r = node.create_equidistant_nodes((settings.max_polynomial_degree // 2) + 2,
                                                       mesh.nodes[len(mesh.nodes) - 1].position,
                                                       mesh.nodes[len(mesh.nodes) - 1].position + (
                                                               settings.ending_point - settings.starting_point) * (
                                                               settings.overshoot_as_percentage_of_total_computational_domain / 100))
    for i in range(len(temp_ghost_nodes_r) - 1):
        volume = temp_ghost_nodes_r[i + 1].position - temp_ghost_nodes_r[i].position
        midpoint = (temp_ghost_nodes_r[i].position + temp_ghost_nodes_r[i + 1].position) / 2
        temp_cell = cell.Cell(volume, midpoint, integrate.scipy_integration(function, temp_ghost_nodes_r[i].position,
                                                                            temp_ghost_nodes_r[
                                                                                i + 1].position) / volume)  # TODO: consider allowing changing this?
        mesh.ghost_cells.append(temp_cell)
    for i in range(len(temp_ghost_nodes_l) - 1):
        volume = temp_ghost_nodes_l[i + 1].position - temp_ghost_nodes_l[i].position
        midpoint = (temp_ghost_nodes_l[i].position + temp_ghost_nodes_l[i + 1].position) / 2
        temp_cell = cell.Cell(volume, midpoint, integrate.scipy_integration(function, temp_ghost_nodes_l[i].position,
                                                                            temp_ghost_nodes_l[
                                                                                i + 1].position) / volume)
        mesh.ghost_cells.append(temp_cell)
    mesh.ghost_nodes = temp_ghost_nodes_r[1:] + temp_ghost_nodes_l[:-1]
    mesh.designate_cells_for_non_global_reconstruction()
    return mesh


def function_to_mesh(function, number_of_nodes: int, starting_point: float,
                     ending_point: float) -> "Mesh":
    """
    Takes a function from interval <starting_point,ending_point> and turns it into a mesh
    :param function: function that is getting turned into a mesh
    :param number_of_nodes: number of nodes in the resulting mesh
    :param starting_point: where the sampling of function starts
    :param ending_point: where the sampling of function end
    :return: resulting mesh
    """
    if ending_point <= starting_point:
        print("Error : Starting point bigger or equal to ending point")
        exit()

    node.create_equidistant_nodes(number_of_nodes, starting_point, ending_point)

    return function_to_mesh_from_node_list(function,
                                           node.create_equidistant_nodes(number_of_nodes, starting_point, ending_point))
