import drawing
import imageio
import mesh
import settings
import settings as s
import node
import os
import writer


def create_gif_wrapper(result_name=None):
    if result_name is None:
        result_name = s.input_function.__name__ + "with" + str(s.how_many_iterations) + "iters " + "and-" + settings.type_of_reconstruction + "-with-max-degree-of" + str(
            settings.max_polynomial_degree) + "from" + str(s.starting_point) + "to" + str(s.ending_point) + "with" + str(
            settings.number_of_nodes - 1) + "cells-beta-" + str(settings.THINC_beta)

    result_path = "./Gifs/" + result_name + ".gif"
    fps = s.how_many_iterations//10  # number of iterations / length of the gif in seconds

    if os.path.exists(result_path):
        os.remove(result_path)

    if not os.path.exists('temp_img'):
        os.mkdir('temp_img')

    gif = imageio.get_writer(result_path)
    input_mesh = mesh.function_to_mesh(s.input_function, s.number_of_nodes, s.starting_point, s.ending_point)
    for i in range(s.how_many_iterations + 1):
        drawing.save(s.plotting_options, input_mesh, 'temp_img/temp.png', s.input_function)
        image = imageio.imread('./temp_img/temp.png')
        gif.append_data(image)
        input_mesh.remap(node.create_nodes_for_cyclic_remapping(i, s.how_many_iterations, s.number_of_nodes))
    writer.write_out_results_and_settings(input_mesh, s.input_function)