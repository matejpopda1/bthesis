from singlematerial import test_functions
from singlematerial import integrate
import random


# domain setting
starting_point = -1
ending_point = 1


# mesh settings
number_of_nodes = 20

# how many times to remap
how_many_iterations = 160

# graphing settings
number_of_samples_for_graph = 1000  # only used for graphing the original function and pointwise plotting
draw_how_many_times = 3


# which graphs to draw in this order (ie. the first item is drawn first)
plotting_options = [  # possible options are included in drawing.draw
    "midpoints",
    # "steps",
    # "slopes",
    # "nodes",
    "simple",
    # "nodes2"
]

# type of reconstruction in non-marked cell
type_of_reconstruction = "P"
# type_of_reconstruction = "P+BJ"

# degree of polynomial in the reconstruction
max_polynomial_degree = 2

# input function
input_function = test_functions.lin_lin_jump





use_limiter_at_selected_cells = True


non_polynomial_reconstruction_type = "THINC"

do_non_polynomial_reconstruction = True

THINC_beta = 10

reconstruction_type_in_cells_next_to_a_discontinuity = ["P+BJ", 1]


# what integration method should be used
integration_method = integrate.scipy_integration
##########################################################
# ghost nodes settings
overshoot_as_percentage_of_total_computational_domain = 20



# set seed for debugging
random.seed(51)


# Checks throughout the code for obvious mistakes (mostly for debugging should not be necessary)
do_tests = False



#########################################################################

draw_after_how_many_iterations = how_many_iterations//draw_how_many_times

global_reconstruction_type = [type_of_reconstruction, max_polynomial_degree]

non_polynomial_reconstruction_type = [non_polynomial_reconstruction_type]

