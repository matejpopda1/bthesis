import settings
import scipy.integrate as scipy
import numpy

def integrate_mesh(mesh, a, b):
    """
    Wrapper method for integrating
    :param mesh: what mesh is getting integrated
    :param a: starting point of the interval
    :param b: ending point of the interval
    """
    resulting_function = None
    if settings.integration_method == exact_integration:
        return settings.integration_method(mesh, a, b)
    elif settings.integration_method == scipy_integration:
        """
        This specific case reduces the amount of calls to the mesh.value_at_x function by a significant factor
        In a test run it was by a factor of 7
        """
        points = []
        for i in mesh.nodes:
            points.append(i.position)

        if hasattr(settings.input_function, "list_of_discontinuities"):
            for i in settings.input_function.list_of_discontinuities:
                for e in numpy.linspace(-0.1, 0.1, 100):
                    points.append(i+e)
        # print(points)
        return settings.integration_method(mesh.value_at_x, a, b, points=points)
    else:
        raise NotImplementedError("Selected integration method is not implemented")


def scipy_integration(function, a, b, points=None):
    """
    integration using the scipy function quad
    """
    if points is None:
        points = []

    x = scipy.quad(function, a, b, points=points, limit=10000, epsabs=10**-10)
    # x = scipy.romberg(function, a, b, divmax=28)
    # return x
    return x[0]


def exact_integration(input_mesh, a, b):
    """
    analytical integration, works only for level of polynomial reconstruction of at most 1
    :param input_mesh: input mesh
    :param a: starting point of the interval
    :param b: ending point of the interval
    :return: integral of the mesh between a and b
    """
    if not (settings.starting_point <= a < b <= settings.ending_point):
        print("Error, bounds of the integral are incorrect")
    result = 0
    # Could be removed, but makes this function 3x faster. Could be replaced with a faster search for intersecting cells (binary search?)
    index_of_intersecting_cells = []
    for i in range(len(input_mesh.cells)):
        if input_mesh.nodes[i + 1].position >= a and input_mesh.nodes[i].position <= b:
            index_of_intersecting_cells.append(i)

    def integral_over_one_cell(lower_bound, upper_bound, index):
        i_1 = (upper_bound - lower_bound)
        i_x = (pow(upper_bound, 2) - pow(lower_bound, 2))/2
        return input_mesh.cells[index].value * i_1 + input_mesh.cells[index].derivative[0] * (i_x - input_mesh.cells[index].mid_point * i_1)

    for i in index_of_intersecting_cells:
        x_0 = max(a, input_mesh.nodes[i].position)
        x_1 = min(b, input_mesh.nodes[i+1].position)
        if x_0 < x_1:
            result += integral_over_one_cell(x_0, x_1, i)

    return result

