import matplotlib.pyplot as plt
import json













if __name__ == "__main__":

    mm = json.load(open("sourcequadquad/mm.json"))
    t5 = json.load(open("sourcequadquad/5.json"))
    t10 = json.load(open("sourcequadquad/10.json"))
    jump = json.load(open("sourcequadquad/jump.json"))


    fig, ax = plt.subplots()
    ax.set_xlabel("x")
    ax.set_ylabel("f(x)")

    ax.plot(jump['analytic function'][0], jump['analytic function'][1], c="black", label="Původní funkce")

    current_x = None
    temp_xx = []
    temp_yy = []
    for i in t10['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="green", label="THINC10" )
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in t5['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="red", label="THINC5")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in mm['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="pink", label="Multimateriálová rekonstrukce")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in jump['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="blue", label="Rekonstrukce nespojitou funkcí")
    # ax.set_xlim(-0.25,0.25)
    plt.legend(bbox_to_anchor=(0.5, 0.9))
    plt.savefig("custom-plots/quadquad.svg")










    lim = json.load(open("realresults/4shape-80nodes/4shape-lim/RawOutput.json"))
    poly = json.load(open("realresults/4shape-80nodes/4shape-poly/RawOutput.json"))
    t5 = json.load(open("realresults/4shape-80nodes/4shape-THINC5/RawOutput.json"))
    t10 = json.load(open("realresults/4shape-80nodes/4shape-THINC10/RawOutput.json"))
    jump = json.load(open("realresults/4shape-80nodes/4shape-JUMP/RawOutput.json"))


    fig, ax = plt.subplots()
    ax.set_xlabel("x")
    ax.set_ylabel("f(x)")


    ax.plot(jump['analytic function'][0], jump['analytic function'][1], c="black", label="Původní funkce")

    current_x = None
    temp_xx = []
    temp_yy = []
    for i in t10['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="green", label="THINC10" )
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in t5['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="red", label="THINC5")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in lim['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="yellow", label="Lineární rekonstrukce s BJ limiterem")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in jump['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="blue", label="Rekonstrukce nespojitou funkcí")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in poly['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="purple", label="Rekonstrukce polynomem 4. stupně")

    # plt.gca().set_aspect('equal')
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.5))

    plt.tight_layout()
    plt.savefig("custom-plots/shape.svg")





    t1 = json.load(open("betashape/r1.json"))
    t5 = json.load(open("betashape/r5.json"))
    t10 = json.load(open("betashape/r10.json"))


    fig, ax = plt.subplots()
    ax.set_xlabel("x")
    ax.set_ylabel("f(x)")


    ax.plot(jump['analytic function'][0], t1['analytic function'][1], c="black", label="Původní funkce")

    current_x = None
    temp_xx = []
    temp_yy = []
    for i, cells in enumerate(t1['simple']):
        temp_xx = cells[0]
        temp_yy = cells[1]
        if i==2:
            label = r"THINC with $\beta=1$"
        else:
            label = None

        ax.plot(temp_xx, temp_yy, color="green", label=label)
    current_x = None
    temp_xx = []
    temp_yy = []
    for cells in t5['simple']:
        temp_xx += cells[0]
        temp_yy += cells[1]
    ax.plot(temp_xx, temp_yy, color="blue", label=r"THINC with $\beta=5$")
    current_x = None
    temp_xx = []
    temp_yy = []
    for cells in t10['simple']:
        temp_xx += cells[0]
        temp_yy += cells[1]
    ax.plot(temp_xx, temp_yy, color="purple", label=r"THINC with $\beta=10$")

    #plt.gca().set_aspect('equal')
    plt.legend()

    ax.set_xlim(-0.25,0.25)

    #plt.tight_layout()
    plt.savefig("custom-plots/thinc.svg")







    print("done")




    s20 = json.load(open("../ALE_reworked/Results/sod-P4JUMP-ale-20-equidistant/raw_output.json"))
    s40 = json.load(open("../ALE_reworked/Results/sod-P4JUMP-ale-40-equidistant/raw_output.json"))
    s80 = json.load(open("../ALE_reworked/Results/sod-P4JUMP-ale-80-equidistant/raw_output.json"))
    s160 = json.load(open("../ALE_reworked/Results/sod-P4JUMP-ale-160-equidistant/raw_output.json"))
    s320 = json.load(open("../ALE_reworked/Results/sod-P4JUMP-ale-320-equidistant/raw_output.json"))


    fig, ax = plt.subplots()
    ax.set_xlabel("x")
    ax.set_ylabel(r"$\rho(x)$")


    # ax.plot(jump['analytic function'][0], jump['analytic function'][1], c="black", label="Původní funkce")


    ax.plot(s20["points"], s20["density"], color="green", label="ALE s nespojitou rekonstrukcí, n = 20" )

    ax.plot(s40["points"], s40["density"], color="red", label="ALE s nespojitou rekonstrukcí, n = 40")

    ax.plot(s80["points"], s80["density"], color="yellow", label="ALE s nespojitou rekonstrukcí, n = 80")

    ax.plot(s160["points"], s160["density"], color="blue", label="ALE s nespojitou rekonstrukcí, n = 160")

    ax.plot(s320["points"], s320["density"], color="purple", label="ALE s nespojitou rekonstrukcí, n = 320")

    # plt.gca().set_aspect('equal')
    # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.legend(loc=1)
    # plt.tight_layout()
    plt.savefig("custom-plots/sodnumberofcells.svg")





    lin = json.load(open("quadraticmap/lin.json"))
    lim = json.load(open("quadraticmap/lim.json"))
    poly = json.load(open("quadraticmap/poly.json"))
    const = json.load(open("quadraticmap/const.json"))


    fig, ax = plt.subplots()
    ax.set_xlabel("x")
    ax.set_ylabel("f(x)")

    ax.plot(jump['analytic function'][0], poly['analytic function'][1], c="black", label="Původní funkce")

    current_x = None
    temp_xx = []
    temp_yy = []
    for i in const['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="green", label="Rekonstrukce konstatní funkcí" )
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in lim['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="red", label="Rekonstrukce lineární funkcí s limiterem")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in lin['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="orange", label="Rekonstrukce lineární funkcí")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in poly['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="blue", label="Rekonstrukce polynomem")

    plt.legend()
    plt.savefig("custom-plots/quadmap.svg")



    lin = json.load(open("sinremap/lin.json"))
    lim = json.load(open("sinremap/lim.json"))
    poly = json.load(open("sinremap/poly.json"))
    const = json.load(open("sinremap/const.json"))


    fig, ax = plt.subplots()
    ax.set_xlabel("x")
    ax.set_ylabel("f(x)")

    ax.plot(jump['analytic function'][0], poly['analytic function'][1], c="black", label="Původní funkce")

    current_x = None
    temp_xx = []
    temp_yy = []
    for i in const['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="green", label="Rekonstrukce konstatní funkcí" )
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in lim['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="red", label="Rekonstrukce lineární funkcí s limiterem")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in lin['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="orange", label="Rekonstrukce lineární funkcí")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in poly['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="blue", label="Rekonstrukce polynomem")

    plt.legend(loc=1)
    plt.savefig("custom-plots/sinremap.svg")








    lim = json.load(open("realresults/jump/4shape-lim/RawOutput.json"))
    poly = json.load(open("realresults/jump/4shape-poly/RawOutput.json"))
    const = json.load(open("realresults/jump/4shape-const/RawOutput.json"))
    t10 = json.load(open("realresults/jump/4shape-THINC10/RawOutput.json"))
    jump = json.load(open("realresults/jump/4shape-JUMP/RawOutput.json"))


    fig, ax = plt.subplots()
    ax.set_xlabel("x")
    ax.set_ylabel("f(x)")


    ax.plot(jump['analytic function'][0], jump['analytic function'][1], c="black", label="Původní funkce")

    current_x = None
    temp_xx = []
    temp_yy = []
    for i in t10['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="green", label="THINC10" )
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in const['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="red", label="Konstantní rekonstrukce")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in lim['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="orange", label="Lineární rekonstrukce s BJ limiterem")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in jump['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="blue", label="Rekonstrukce nespojitou funkcí")
    current_x = None
    temp_xx = []
    temp_yy = []
    for i in poly['midpoints']:
        if current_x is None or i[0] > current_x:
            temp_xx.append(i[0])
            current_x = i[0]
            temp_yy.append(i[1])
    ax.plot(temp_xx, temp_yy, color="grey", label="Rekonstrukce polynomem 4. stupně")

    # plt.gca().set_aspect('equal')
    plt.legend(loc='lower right', bbox_to_anchor=(1.3, 0.0))
    # plt.legend(loc='center left')
    plt.tight_layout()
    plt.savefig("custom-plots/jump.svg")



    s20 = json.load(open("refugees/sod-ale-80-equidistantjump/raw_output.json"))
    s40 = json.load(open("refugees/sod-ale-80-equidistantpoly/raw_output.json"))
    s80 = json.load(open("refugees/sod-ale-80-equidistantthinc5/raw_output.json"))
    s160 = json.load(open("refugees/sod-lagrange-80-equidistant/raw_output.json"))


    fig, ax = plt.subplots()
    ax.set_xlabel("x")
    ax.set_ylabel(r"$\rho(x)$")


    # ax.plot(jump['analytic function'][0], jump['analytic function'][1], c="black", label="Původní funkce")


    ax.plot(s20["points"], s20["density"], color="green", label="ALE režim s nespojitou rekonstrukcí" )

    ax.plot(s40["points"], s40["density"], color="red", label="ALE režim s polynomiální rekonstrukcí")

    ax.plot(s80["points"], s80["density"], color="orange", label=r"ALE režim s THINC, $\beta = 10$")

    ax.plot(s160["points"], s160["density"], color="blue", label="Lagrangeovský režim")

    # plt.gca().set_aspect('equal')
    # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    ax.set_xlim(0.6,0.8)
    plt.legend(loc=1)
    # plt.tight_layout()
    plt.savefig("custom-plots/sodvariation.svg")




    s20 = json.load(open("refugees/so-ale-80-equidistantjump/raw_output.json"))
    s40 = json.load(open("refugees/so-ale-80-equidistant-poly/raw_output.json"))
    s160 = json.load(open("refugees/so-lagrange-80-equidistant/raw_output.json"))


    fig, ax = plt.subplots()
    ax.set_xlabel("x")
    ax.set_ylabel(r"$\rho(x)$")


    # ax.plot(jump['analytic function'][0], jump['analytic function'][1], c="black", label="Původní funkce")


    ax.plot(s20["points"], s20["density"], color="green", label="ALE režim s nespojitou rekonstrukcí" )

    ax.plot(s40["points"], s40["density"], color="red", label="ALE režim s polynomiální rekonstrukcí")

    ax.plot(s160["points"], s160["density"], color="blue", label="Lagrangeovský režim")

    # plt.gca().set_aspect('equal')
    # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.legend(loc=3)
    # plt.tight_layout()
    plt.savefig("custom-plots/SOvariation.svg")

    ax.set_xlabel("x")
    ax.set_ylabel("f(x)")