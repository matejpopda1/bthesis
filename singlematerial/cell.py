from typing import List


class Cell:
    number_of_instances = 0

    def __init__(self, volume: float, mid_point: float, value: float = 0):
        """
        :param volume: volume of a cell
        :param mid_point: mid point of a cell
        :param value: value of the cell
        """
        self.volume = volume
        self.mid_point = mid_point
        self.value = value

        self.reconstruction_type = []
        """
        for polynomial reconstruction: 
        reconstruction_type = ["P", n], where n is the degree of the polynomial
        """

        self.derivative: List[float] = [0]  # list of derivatives, starting at the first derivative
        self.params_for_other_reconstructions = {}
        self.conservation_constants: List[float] = [0, 0]  # list of conservation constant starting at k_0, -> first 2 values are always supposed to be 0

        Cell.number_of_instances += 1

    def __del__(self):
        Cell.number_of_instances -= 1
