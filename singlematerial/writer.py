import matplotlib.pyplot as plt
import drawing
import mesh
import settings as s
import pandas as pd
import node
import test_functions


def save_figure(options: list, input_mesh: mesh.Mesh, name: str = "figure"):
    fig, ax = plt.subplots()

    # selects only values that are both in possible_options and options
    possible_options = []
    for i in range(len(options)):
        possible_options.append(drawing.all_draw_options.get(options[i]))
    for i in possible_options:
        i(input_mesh, ax)

    plt.savefig(name)


def write_out_results_and_settings(input_mesh: mesh.Mesh, input_function, path=None):
    result = {}
    result["Number_of_iterations"] = s.how_many_iterations
    result["Maximum_degree_of_polynomial_in_reconstruction"] = s.global_reconstruction_type[1]
    result["Start_of_computational_domain"] = str(s.starting_point)
    result["End_of_computational_domain"] = str(s.ending_point)
    result["Number_of_nodes"] = s.number_of_nodes
    result["Original_function"] = input_function.__name__
    result["Original_function"] = input_function.__doc__
    result["L1_error"] = input_mesh.l1_error(input_function)
    result["L2_error"] = input_mesh.l2_error(input_function)
    result["L_inf_error"] = input_mesh.l_inf_error(input_function)
    result["max_error"] = input_mesh.max_error(input_function)
    result["Integration_method"] = s.integration_method.__name__
    result["Integration_method_description"] = s.integration_method.__doc__
    result["Used global type of reconstruction"] = s.global_reconstruction_type[0]

    if path is None:
        try:
            df = pd.read_excel("results.xlsx")
            df = df.append(result, ignore_index=True)
            print("With {nodes} nodes we achieve errors: {l1} for the L_1 error, {l2} for the L_2 error, "
                  "{l_inf} for the L_inf and {max} for the max error".format(nodes=result["Number_of_nodes"],
                                                                             l1=result["L1_error"],
                                                                             l2=result["L2_error"],
                                                                             l_inf=result["L_inf_error"],
                                                                             max=result["max_error"]))
        except FileNotFoundError:
            df = pd.DataFrame(result, index=[0])
        df.to_excel("results.xlsx", index=False)
    else:
        df = pd.DataFrame(result, index=[0])
        df.to_json(path + "/result.json")


def calculate_multiple_results(list_of_how_many_nodes, draw_at_the_end=True):
    for j in list_of_how_many_nodes:
        s.number_of_nodes = j
        temp_mesh = mesh.function_to_mesh(s.input_function, s.number_of_nodes, s.starting_point, s.ending_point)
        for i in range(s.how_many_iterations):
            # temp_mesh.remap(node.create_random_nodes(s.number_of_nodes))
            temp_mesh.remap(node.create_nodes_for_cyclic_remapping(i, s.how_many_iterations, s.number_of_nodes))
        if draw_at_the_end:
            drawing.draw(s.plotting_options, temp_mesh, s.input_function)
        write_out_results_and_settings(temp_mesh, s.input_function)


def writer_wrapper(draw_at_the_end=True):
    functions_to_draw = [
        test_functions.jump,
        test_functions.linear,
        test_functions.quadratic,
        test_functions.sine
    ]

    iterations_to_try = [50, 100, 200]

    reconstructions_to_try = [
        ["P", 0],
        ["P", 1],
        ["P+BJ", 1]
    ]

    how_many_nodes = [10, 20, 40, 80, 160]
    # how_many_nodes = [40, 80]

    for i in iterations_to_try:
        s.how_many_iterations = i
        for f in functions_to_draw:
            s.input_function = f
            for r in reconstructions_to_try:
                s.global_reconstruction_type = r
                calculate_multiple_results(how_many_nodes, draw_at_the_end=draw_at_the_end)
