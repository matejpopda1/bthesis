import os

all_paths = [


    # "results/const_const_thinc1",
    # "results/const_const_thinc5",
    # "results/const_const_thinc10",
    # "results/lin_lin_thinc10",
    #
    # "results/BJrecOnJump",
    # "results/BJrecOnSin",
    # "results/constrecOnjump",
    # "results/constrecOnlinear",
    # "results/const_const_jump",
    # "results/jumpsBJ",
    # "results/jumpsNotBJ",
    #
    # "results/lin_lin_jump",
    #
    # "results/linrecOnJump",
    #
    # "results/linrecOnSin",
    #
    # "results/polyrecOnJump",
    #
    # "results/polyrecOnSin",

    # "results/4shape-JUMP",
    # "results/4shape-lim",
    # "results/4shape-poly",
    # "results/4shape-THINC5",
    # "results/4shape-THINC10"

    "results/4shape-lin",
    # "results/4shape-lim",
    # "results/4shape-poly",
    "results/4shape-const"
]

if __name__ == "__main__":
    for path in all_paths:
        print("main.py " + path)
        os.system("python main.py " + path)

    print("done")

