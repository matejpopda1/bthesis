import sys
import drawing
import settings
from singlematerial import mesh
import node
from singlematerial.writer import write_out_results_and_settings

import test_functions

def results_from_options_at_path(options_path):
    """
    options_path: directory containing options.py (doesnt end with /)
    draws the final graph and saves some stats in the same folder
    """

    plotting_options = [  # possible options are included in drawing.draw
        "midpoints",
        # "steps",
        # "slopes",
        # "nodes",
        "simple",
        # "nodes2"
    ]



    sys.path.append(options_path)
    import options as s
    settings.input_function = s.input_function
    settings.non_polynomial_reconstruction_type = s.non_polynomial_reconstruction_type
    settings.plotting_options                                         =plotting_options
    settings.use_limiter_at_selected_cells                            =s.use_limiter_at_selected_cells
    settings.type_of_reconstruction                                   =s.type_of_reconstruction
    settings.THINC_beta                                               =s.THINC_beta
    settings.starting_point                                           =s.starting_point
    settings.reconstruction_type_in_cells_next_to_a_discontinuity     =s.reconstruction_type_in_cells_next_to_a_discontinuity
    settings.overshoot_as_percentage_of_total_computational_domain    =s.overshoot_as_percentage_of_total_computational_domain
    settings.number_of_samples_for_graph                              =s.number_of_samples_for_graph
    settings.max_polynomial_degree                                    =s.max_polynomial_degree
    # settings.integration_method                                       =s.integration_method
    settings.input_function                                           =test_functions.jump
    settings.how_many_iterations                                      =300
    settings.global_reconstruction_type                               =s.global_reconstruction_type
    settings.ending_point                                             =s.ending_point
    settings.draw_how_many_times                                      =s.draw_how_many_times
    settings.draw_after_how_many_iterations                           =s.draw_after_how_many_iterations
    # settings.do_tests                                                 =s.do_tests
    settings.do_non_polynomial_reconstruction                         =s.do_non_polynomial_reconstruction
    settings.number_of_nodes                                          =40


    temp_mesh = mesh.function_to_mesh(settings.input_function, settings.number_of_nodes, s.starting_point, s.ending_point)

    drawing.save(settings.plotting_options, temp_mesh, options_path, settings.input_function, "/RawInput.json", options_path.replace("results/",""))

    for i in range(settings.how_many_iterations + 1):
        # if i % 1 == 0:
        #     drawing.save(s.plotting_options, temp_mesh, options_path, s.input_function, "/RawInput.json", "it"+str(i))
        temp_mesh.remap(node.create_nodes_for_cyclic_remapping(i, settings.how_many_iterations, settings.number_of_nodes, starting_point=settings.starting_point, ending_point=settings.ending_point))

    drawing.save(settings.plotting_options, temp_mesh, options_path, settings.input_function, "/RawOutput.json")
    write_out_results_and_settings(temp_mesh, settings.input_function, options_path)

