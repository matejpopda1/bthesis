import settings
import mesh
import matplotlib.pyplot as plt
import numpy as np
import node
import json





"""
This module takes care of drawing graphs, they are wrapped in the draw function :

Quick description of the functions

plot_steps: plots step graph
plot_midpoints: plots midpoints
plot_slopes: plots linear reconstruction of the mesh
plot_node: plots nodes


"""

# plotting functions


def plot_steps(input_mesh: mesh.Mesh, ax: plt.Axes):
    result = [[input_mesh.nodes[0].position], [input_mesh.cells[0].value]]
    for i in range(len(input_mesh.cells)):
        result[0].append(input_mesh.nodes[i + 1].position)
        result[1].append(input_mesh.cells[i].value)
    ax.step(result[0], result[1], where="pre", color="r")


def plot_midpoints(input_mesh: mesh.Mesh, ax: plt.Axes, output_raw=None):
    result = [[], []]
    for i in input_mesh.cells:
        result[0].append(i.mid_point)
        result[1].append(i.value)
        if output_raw is not None:
            output_raw['midpoints'].append([i.mid_point,  i.value, 'mat0'])
    ax.scatter(result[0], result[1], color="forestgreen", label="Střední hodnoty buněk")


def plot_slopes(input_mesh: mesh.Mesh, ax: plt.Axes):
    input_mesh.reconstruction()
    for i in range(len(input_mesh.cells)):
        xx = [input_mesh.nodes[i].position, input_mesh.nodes[i + 1].position]
        run = input_mesh.cells[i].volume
        yy = [input_mesh.cells[i].value - run * input_mesh.cells[i].derivative[0] / 2,
              input_mesh.cells[i].value + run * input_mesh.cells[i].derivative[0] / 2]
        ax.plot(xx, yy, c="black")


def plot_nodes(input_mesh: mesh.Mesh, ax: plt.Axes, dummy):
    result = []
    for i in range(len(input_mesh.nodes)):
        result.append(input_mesh.nodes[i].position)
    ax.scatter(result, np.zeros(len(result)), color="cyan")


def plot_pointwise(input_mesh: mesh.Mesh, ax: plt.Axes, output_raw=None):
    input_mesh.reconstruction()
    for i in range(len(input_mesh.cells)):
        number_of_samples_in_cell = settings.number_of_samples_for_graph // settings.number_of_nodes
        if input_mesh.cells[i].reconstruction_type == ["THINC"]:
            number_of_samples_in_cell = 1000
        xx = np.linspace(input_mesh.nodes[i].position + 0.00005, input_mesh.nodes[i + 1].position - 0.00005, number_of_samples_in_cell)
        yy = np.zeros(number_of_samples_in_cell)
        for j in range(number_of_samples_in_cell):
            yy[j] = input_mesh.value_at_x(xx[j])
        if output_raw is not None:
            output_raw['simple'].append([xx.tolist(),  yy.tolist(), 'mat0', "cell" + str(i)])

        label = None
        if i == 3:
            label = "Rekonstrukce"

        ax.plot(xx, yy, c="g", label=label)


def plot_cell_borders(input_mesh: mesh.Mesh, ax: plt.Axes, output_raw=None):
    for i in range(len(input_mesh.nodes)):
        ax.axvline(input_mesh.nodes[i].position, color="black")
        if output_raw is not None:
            output_raw['nodes2'].append(input_mesh.nodes[i].position)

# setup
all_draw_options = {
    "midpoints": plot_midpoints,
    "steps": plot_steps,
    "slopes": plot_slopes,
    "nodes": plot_nodes,
    "simple": plot_pointwise,
    "nodes2": plot_cell_borders
}


def draw(options: list, input_mesh: mesh.Mesh, function=None):
    """
    :param options: List of any combination of the following options "midpoints", "steps", "slopes", "nodes", "simple"
    :param input_mesh: mesh that is getting drawn
    :param function: optional parameter for drawing the analytic function
    :return: void
    """
    fig, ax = plt.subplots()
    graph_domain = np.linspace(settings.starting_point, settings.ending_point, settings.number_of_samples_for_graph)
    graph_range = np.zeros(settings.number_of_samples_for_graph)

    # selects only values that are both in possible_options and options
    possible_options = []
    for i in range(len(options)):
        possible_options.append(all_draw_options.get(options[i]))

    if function is not None:
        for i in range(settings.number_of_samples_for_graph):
            graph_range[i] = function(graph_domain[i])
        ax.plot(graph_domain, graph_range, color="b")


    for i in possible_options:
        i(input_mesh, ax)

    plt.show()


def draw_from_global_settings(input_mesh: mesh.Mesh):
    """
    Will get deprecated in the future
    :param input_mesh:
    :return:
    """

    for i in range(settings.how_many_iterations):
        # mesh.remap(node.create_random_nodes(s.number_of_nodes))
        input_mesh.remap(
            node.create_nodes_for_cyclic_remapping(i, settings.how_many_iterations, settings.number_of_nodes))
        if i % settings.draw_after_how_many_iterations == 0:
            draw(settings.plotting_options, input_mesh, settings.input_function)
    if (settings.how_many_iterations - 1) % settings.draw_after_how_many_iterations != 0:
        draw(settings.plotting_options, input_mesh, settings.input_function)


def save(options: list, input_mesh: mesh.Mesh, path, function=None, file_name="RawData.json", image_name="result"):
    """
    :param options: List of any combination of the following options "midpoints", "steps", "slopes", "nodes", "simple"
    :param path: path to the save file
    :param input_mesh: mesh that is getting drawn
    :param function: optional parameter for drawing the analytic function
    :return: void
    """
    fig, ax = plt.subplots()
    graph_domain = np.linspace(settings.starting_point, settings.ending_point, settings.number_of_samples_for_graph)
    graph_range = np.zeros(settings.number_of_samples_for_graph)

    points_for_drawing = {}

    ax.set_xlabel("x")
    ax.set_ylabel(r'$f_i$')

    # selects only values that are both in possible_options and options
    possible_options = []
    for i in range(len(options)):
        possible_options.append(all_draw_options.get(options[i]))
        points_for_drawing[options[i]] = []

    if function is not None:
        for i in range(settings.number_of_samples_for_graph):
            graph_range[i] = function(graph_domain[i])
        ax.plot(graph_domain, graph_range, color="b", label="Původní funkce")
        points_for_drawing["analytic function"] = [graph_domain.tolist(), graph_range.tolist()]

    for i in possible_options:
        i(input_mesh, ax, points_for_drawing)


    ax.legend(loc=2)

    json.dump(points_for_drawing, open(path + file_name, 'w'))
    plt.savefig(path + "/" + image_name + ".svg", format='svg', dpi=1200)

    plt.close()
