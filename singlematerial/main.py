import sys

import settings as s
import mesh
import drawing
import writer
import saver


if __name__ == "__main__":

    # gif.create_gif_wrapper()
    saver.results_from_options_at_path(sys.argv[1])

    # saver.results_from_options_at_path("results/const_const_jump")
    # saver.results_from_options_at_path("results/const_const_thinc5")
    # saver.results_from_options_at_path("results/const_const_thinc15")
    # saver.results_from_options_at_path("results/lin_lin_jump")
    # saver.results_from_options_at_path("results/lin_lin_thinc10")



    # mesh = mesh.function_to_mesh(s.input_function, s.number_of_nodes, s.starting_point, s.ending_point)
    # drawing.draw_from_global_settings(mesh)
    # writer.write_out_results_and_settings(mesh, s.input_function)

    # writer.save_figure(s.plotting_options, mesh)

    # writer.calculate_multiple_results([20, 40, 80, 160])

    # writer.writer_wrapper(draw_at_the_end=False)

    print("done")
