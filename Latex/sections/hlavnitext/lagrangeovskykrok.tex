\documentclass[../../main.tex]{subfiles}
\graphicspath{{\subfix{../../images/}}}
\begin{document}

\section{Lagrangeovský krok ALE metod}

Pro Lagrangeovský krok jsme zvolily cell-centered metodu, toto nám umožňuje remapovat všechny proměnné stejně. 

\subsection{Fyzikální popis}


V 1D hydrodynamice popisujeme pohyb stlačitelné tekutiny následujícími Eulerovými rovnicemi. 

Předpokládáme fluidní částici.\cite{kucharik_high-accurate_2020}
Označme její počáteční pozici jako $X$ a označme její pozici v čase $t$ jako $x(X, t)$. X označuje její Lagrangeovu souřadnici. 
Známe-li funkci $u = u(x, t)$, udávající rychlost tekutiny, tak pak celou trajektorii můžeme spočítat ze vztahu 
\begin{equation}\label{eq:movementdefinition}
    \frac{\partial x}{\partial t} = u, \hfil x(X, 0) = X,
\end{equation}
kde $x$ udává Eulerovu souřadnici.

1D systém Eulerovských rovnic pro hydrodynamiku lze zapsat jako 

\begin{equation}\label{eq:eulerCont1}
    \rho \derivativeTime \frac{1}{\rho} - \pderivativeX u = 0
\end{equation}
\begin{equation}\label{eq:eulerCont2}
    \rho \derivativeTime u + \pderivativeX p = 0
\end{equation}
\begin{equation}\label{eq:eulerCont3}
    \rho \derivativeTime E + \pderivativeX (pu) = 0
\end{equation}

kde $\rho$ označuje hustotu, $p$ tlak, $u$ rychlost a $E$ měrnou celkovou energii. Dále symbolem $\derivativeTime$ rozumíme totální (v literatuře také nazývanou jako materiálovou) derivaci.
 
Pro uzavření systému potřebujeme ještě jednu rovnici. K tomu slouží stavová rovnice ideálního plynu.
\begin{equation}\label{eq:ofState}
    p = P(\rho_i, \varepsilon_i) = \rho\varepsilon(\gamma - 1),
\end{equation}
kde $\gamma$ je adiabatická konstanta a $\varepsilon$ měrná vnitřní energie $\varepsilon = E - \frac{1}{2} \| u \|^2 $.


\subsection{Diskretizace prvního řádu}

V síti definované v sekci \ref{sec:sitka} je pohyb uzlu $x_{i+\half}$ popsán podle \eqref{eq:movementdefinition} jako
\begin{equation}
    \frac{\partial x_{i+\half}}{\partial t} = u_{i+\half}, \hfil x_{i+\half}(0) = X_{i+\half}
\end{equation}

Označme střední hodnotu hustoty jako $\rho_i$, střední hodnotu rychlosti jako $u_i$ a střední hodnotu měrné celkové energie $E_i$. 

Integrujeme systém rovnic \eqref{eq:eulerCont1} \eqref{eq:eulerCont2} \eqref{eq:eulerCont3}, 
to spolu s vlastností Lagrangeovské formulace, kdy je hmota buňky konstantní (tj. $m_i = \rho_i(t) V_i(t), \forall t$), dává \cite{maire_high-order_2009}:

\begin{equation}\label{eq:eulerdc1}
    m_i \derivativeTime \frac{1}{\rho_i} + u_{i-\half} - u_{i+\half} = 0
\end{equation}

\begin{equation}\label{eq:eulerdc2}
    m_i \derivativeTime u_i - p_{i-\half} + p_{i+\half} = 0
\end{equation}

\begin{equation}\label{eq:eulerdc3}
    m_i \derivativeTime E_i - p_{i-\half}u_{i-\half} + p_{i+\half}u_{i+\half} = 0
\end{equation}
kde $u_{i-\half}$ a $p_{i-\half}$ označují toky uzly. 


Tyto toky můžeme dostat řešením Riemannova problému na rozhraní mezi buňkami. Na levé straně uzlu máme stav $\frac{1}{\rho_i}$, $u_i$, $E_i$, na pravé $\frac{1}{\rho_{i+1}}$, $u_{i+1}$, $E_{i+1}$.
Pro tuto aproximaci používáme Godunovův akustický řešič \cite{godunov1979resolution}.
Definujeme akustickou impedanci $z_{i} = \rho_{i} a_{i}$, kde $a$ označuje rychlost zvuku.
Řešení Riemannova problému je pak dáno následovně: 

\begin{equation}
    u_{i+\half} = \frac{z_i u_i + z_{i+1} u_{i+1}}{z_i + z_{i+1}} - \frac{p_{i+1} - p_i}{z_i + z_{i+1}}
\end{equation}

\begin{equation}
    p_{i+\half} = \frac{z_i p_{i+1} + z_{i+1} p_{i}}{z_i + z_{i+1}} - \frac{z_iz_{i+1}(u_{i+1} - u_i)}{z_i + z_{i+1}}
\end{equation}


Provedeme diskretizaci rov\-nic \eqref{eq:eulerdc1} \eqref{eq:eulerdc2} \eqref{eq:eulerdc3} v ča\-se.
Známe všechny proměnné v čase $t=t^n$. Definujeme $\Delta t = t^{n+1} - t^n$.

Poté nahradíme derivace pro každou veličiny dopřednými diferencemi 
\begin{equation}
    \frac{\partial g(t)}{\partial t} \approx \frac{g^{n+1} - g^{n}}{\Delta t}
\end{equation}

Dostáváme


\begin{equation}\label{eq:ediscret1}
    m_i \left(\frac{1}{\rho^{n+1}_i}  - \frac{1}{\rho^{n}_i}\right) + \Delta t \left( u^n_{i-\half} - u^n_{i+\half}\right) = 0
\end{equation}

\begin{equation}\label{eq:ediscret2}
    m_i \left(u_i^{n+1}  - u_i^{n}\right) - \Delta t\left(p^n_{i-\half} + p^n_{i+\half}\right) = 0
\end{equation}

\begin{equation}\label{eq:ediscret3}
    m_i \left(E_i^{n+1}  - E_i^{n}\right) - \Delta t\left( p^n_{i-\half}u^n_{i-\half} + p^n_{i+\half}u^n_{i+\half} \right) = 0
\end{equation}

Tato sada diskrétních rovnic vede na schéma prvního řádu v čase i prostoru. Pohyb uzlů je pak dán jako
\begin{equation}
    x_{i+\half}^{n+1} = x^n_{i+\half} + \Delta t u^n_{i+\half}
\end{equation}

Dále musíme spočítat novou měrnou vnitř\-ní ener\-gii,
$\varepsilon_{i}^{n+1} = E_i^{n+1}  - \frac{1}{2} (u_{i}^{n+1})^2$.

Nakonec získáme tlak ze stavové rovnice \eqref{eq:ofState}. 

\subsection{Diskretizace druhého řádu}

Vylepšení schématu na 2. řád přesnosti je popsáno v \cite{maire_high-order_2009}.

Předpokládáme, že funkce udávající rychlost v buňce $i$ je lineární, 
stejně jako u eulerovského kroku provádíme rekonstrukci za pomoci nejmenších 
čtverců a sousedních buněk.

Opět nechceme, aby vznikali nové extrémy, znovu tedy používáme limiter. 
Narozdíl od remapu zde používáme Venkatakrishnanův limiter \cite{Venkatakrishnan_1993}, 
který má hladší průběh než BJ limiter.


Rovnice  \eqref{eq:ediscret1} \eqref{eq:ediscret2} \eqref{eq:ediscret3} pak nahradíme rovnicemi středovanými v čase,
\begin{equation}\label{eq:e2discret1}
    m_i \left(\frac{1}{\rho^{n+1}_i}  - \frac{1}{\rho^{n}_i}\right) + \Delta t \left( u^{n+\half}_{i-\half} - u^{n+\half}_{i+\half}\right) = 0
\end{equation}

\begin{equation}\label{eq:e2discret2}
    m_i \left(u_i^{n+1}  - u_i^{n}\right) - \Delta t\left(p^{n+\half}_{i-\half} + p^{n+\half}_{i+\half}\right) = 0
\end{equation}

\begin{equation}\label{eq:e2discret3}
    m_i \left(E_i^{n+1}  - E_i^{n}\right) - \Delta t\left( p^{n+\half}_{i-\half}u^{n+\half}_{i-\half} + p^{n+\half}_{i+\half}u^{n+\half}_{i+\half} \right) = 0
\end{equation}

Pohyb uzlu je pak popsán
\begin{equation}
    x^{n+1}_{i+\half} = x^n_{i+\half} + \Delta t u^{n+\half}_{i+\half}
\end{equation}


\subsection{Velikost časového kroku}

Pro zaručení stability metody volíme velikost časového kroku následovně \cite{maire_high-order_2009}. 

Z CFL podmínky máme 

\begin{equation}
    \Delta t_{CFL} = C_{CFL} \min_i \frac{\Delta x_i^n}{a_i^n},
\end{equation}

kde $C_{CFL}$ je námi určený koeficient větší než 0 a $a_i^n$ je 
rychlost zvuku v buňce $i$. 

Dále použijeme podmínku omezující změnu objemu buňky.
\begin{equation}
    % \frac{|V^{n+1}_i - V^n_i|}{v_i^n} \leq C_V
    \Delta t_{V} \leq C_V * \frac{V^n_i}{|u_{i+\half} - u_{i-\half}|},
\end{equation}
kde $C_V$ je konstanta z intervalu $(0,1)$


Dále používáme omezení na změnu časového kroku
\begin{equation}
    \Delta t_{m} = C_M \Delta t^{n-1},
\end{equation}
kde $C_M$ je námi určená konstanta větší než 1, 
díky které se časový krok postupně zvětšuje.

Nový časový krok je pak určen
\begin{equation}
    t^n = \min(\Delta t_{CFL}, \Delta t_{V}, \Delta t_{m})
\end{equation}


V našem ALE kódu konstanty volíme následovně:\\ $C_{CFL} = 0.25, C_V = 0.1, C_M = 1.01$.


\subsection{Okrajové podmínky}

Protože v našem kódu používáme diskretizaci druhého řádu, která provádí rekonstrukci veličin,
je potřeba aby všechny buňky vedle sebe měly souseda. Toto neplatí na kraji výpočetní oblasti, 
proto opět používáme ghost buňky stejně jako u remapu.

V případě, že simulaci zastavíme před tím, než vlny dorazí na okraj simulované oblasti, jsou
uzly na hranici udržené v klidu díky rovnováze mezi krajní buňkou a sousedící ghost buňkou.

Pokud to problém vyžaduje můžeme rychlost okrajových uzlů 
nastavit ručně. Například pro levý krajní uzel dáváme $u_{-\half} = u_L(t)$, 
pro funkci $u_L(t)= 0 $ se jedná o zeď, 
pro $u_L(t) = at$ se jedná o rovnoměrně zrychlující píst. 


\subsection{Sledování kontaktu}

Protože v Eulerovském kroku provádíme remap za pomoci nespojitých funkcí, 
který můžeme provést pouze na rozhraní, je potřeba označit buňku, ve které se kontaktní nespojitost nachází.



My kontakt sledujeme následovně. Z počátečních podmínek známe pozici kontaktu jako $x^0_c$.
Předpokládáme, že kontakt se nachází v buňce $i$.
Na konci každého Lagrangeovského kroku pak provádíme následující aktualizaci jeho pozice.
\begin{equation}
    x^{n+1}_c = x^n_c + \Delta t \left( \left(1 - \frac{x^n_c - x^n_{i-\half}}{V^n_i}\right) u^{n+\half}_{i-\half} + \frac{x^n_c - x^n_{i-\half}}{V^n_i}  u^{n+\half}_{i+\half}         \right)
\end{equation}
Tedy lineárně interpolujeme rychlost uzlů buňky ve které se kontakt nachází. 
V našem kódu si pro každý kontakt který sledujeme, ukládáme jeho současnou polohu a u každého Eulerovského kroku explicitně zjišťujeme v které buňce se kontakt nachází. 
V Eulerovském kroku pak v buňce, která nespojitost obsahuje, provádíme THINC nebo nespojitou rekonstrukci. 

\subsection{Detekce komprese}

Rázová vlna většinou bývá roztažena přes dvě až čtyři buňky, 
proto není vhodná pro THINC rekonstrukci.
V remapu ale u této nespojitosti nechceme provádět rekonstrukci vysokého řádu, proto si 
buňky, které kompresní vlnu obsahují, potřejeme označit.

K tomu používáme tento detektor
\begin{equation}
    \left( u^{n+\half}_{i+\half} - u_{i-\half}^{n+\half} \right) < (\kappa - 1) \frac{V_i^n}{\Delta t}
\end{equation}
$\kappa$ je námi zvolený parametr, v našem kódu volíme $\kappa = 0.95$.

Pokud je tato nerovnost pravdivá, buňku označíme a u remapu v této buňce používáme
lineární rekosntrukci s limiterem. 















































% \begin{equation}\label{eq:eulerian:1}
%      \pderivativeTime \rho + \pderivativeX (\rho u) = 0
% \end{equation}

% \begin{equation}\label{eq:eulerian:2}
%     \pderivativeTime (\rho u) + \pderivativeX (\rho u^2 + p) = 0
% \end{equation}

% \begin{equation}\label{eq:eulerian:3}
%     \pderivativeTime \rho E + \pderivativeX(\rho E u + p u) = 0
% \end{equation}





% Do rovnice \eqref{eq:eulerian:3} dosaďme \eqref{eq:ofState}

% \begin{equation}\label{eq:eulerian:3b}
%     \pderivativeTime p + \pderivativeX( p u + p u) = 0
% \end{equation}

% Napišme si celý systém ještě jednou ale rozepišme derivace součinu : 

% \begin{equation}\label{eq:eulerian1:1}
%     \pderivativeTime \rho + u \pderivativeX \rho  + \rho \pderivativeX u = 0
% \end{equation}

% \begin{equation}\label{eq:eulerian1:2}
%    \pderivativeTime (\rho u) + \pderivativeX p + u^2 \pderivativeX \rho + \rho \pderivativeX u^2 = 0         %\pderivativeTime (\rho u) + \pderivativeX (\rho u^2 + p) = 0
% \end{equation}

% \begin{equation}\label{eq:eulerian1:3}
%    \pderivativeTime \rho E + \pderivativeX(\rho E u) +  u \pderivativeX p + p  \pderivativeX u = 0     %    \pderivativeTime \rho E + \pderivativeX(\rho E u + p u) = 0
% \end{equation}


% Pro převedení do Lagrangeovského formalismu si vyjádřeme totální časovou derivaci (v literatuře se také setkáváme s pojmem materiálové derivace)

% \begin{equation}
%     \derivativeTime = \pderivativeTime + \pderivativeX \frac{\text{d} x}{\text{d} t}
% \end{equation}

% Po provedení transformace dostáváme následující: 

% \begin{equation}\label{eq:eulerianlagrangiag:1}
%     \pderivativeTime \rho + \pderivativeX (\rho u) = 0
% \end{equation}

% \begin{equation}\label{eq:eulerianlagrangiag:2}
%    \pderivativeTime (\rho u) + \pderivativeX (\rho u^2 + p) = 0
% \end{equation}

% \begin{equation}\label{eq:eulerianlagrangiag:3}
%    \pderivativeTime \rho E + \pderivativeX(\rho E u + p u) = 0
% \end{equation}







% \todo{chybi zacatek}


% Eulerovy rovnice v Lagrangeově formalismu diskretizované v prostoru tedy mají v buňce $i$ integrální tvar. \cite{maire_cell-centered_2007}

% \begin{equation}\label{eq:lagrSys1:1}
%     m_i \derivativeTime \nu_i - \int_{\partial G_i} U \cdot N \dl = 0
% \end{equation}

% \begin{equation}\label{eq:lagrSys1:2}
%     m_i \derivativeTime U_i + \int_{\partial G_i} PN \dl = 0
% \end{equation}

% \begin{equation}\label{eq:lagrSys1:3}
%     m_i \derivativeTime E_i + \int_{\partial G_i} P U \cdot N \dl = 0
% \end{equation}

% kde  $m_i$ udává hmotnost buňkt, $\nu_i$ měrný objem, $U$ rychlost, $N$ normálový vektor, $P$ tlak, $E_i$ měrnou celkovou energii. 

% Dále definujme hustotu $\rho_i = \frac{1}{\nu_i}$. 


% Dále zadefinujme toky, označme $V^* \cdot N$ objemový tok, $p^*\cdot N$ tok hybnosti a $(Pu)^*\cdot N$ jako tok celkové energie. Platí \todo{přepsat}

% \begin{equation}
%     V^* \cdot N = U\cdot N
% \end{equation}

% \begin{equation}
%     p^*\cdot N
% \end{equation}

% \begin{equation}
%     (Pu)^*\cdot N
% \end{equation}



% V 1D systém \eqref{eq:lagrSys1:1}\eqref{eq:lagrSys1:2}\eqref{eq:lagrSys1:3} přechází v

% \begin{equation}
%     m_i \derivativeTime \nu_i + U_{i-\half} - U_{i+\half} = 0
% \end{equation}

% \begin{equation}
%     m_i \derivativeTime U_i - P_{i-\half} + P_{i+\half} = 0
% \end{equation}

% \begin{equation}
%     m_i \derivativeTime E_i + P_{i-\half}U_{i-\half} - P_{i+\half}U_{i+\half} = 0
% \end{equation}


% V této Lagrangeově diskretizaci ještě potřebujeme vzít v potaz pohyb buňek. Zapišme pohyb uzlů jako

% \begin{equation}
%     \derivativeTime x_{i+\half} = u_{i+\half}, x_{i+\half}(0) = x_{0,i+\half}
% \end{equation}



% \subsection{Zákony zachování}

% Celková hybnost systému (bez okrajových podmínek) je dána jako suma přes všechny buňky. 

% \begin{equation}
%     \sum_{i=0}^n m_i u_i = 
% \end{equation}































\end{document}