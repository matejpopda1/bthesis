\babel@toc {czech}{}\relax 
\contentsline {section}{\numberline {1}Úvod}{9}{section.1}%
\contentsline {section}{\numberline {2}ALE metody}{10}{section.2}%
\contentsline {subsection}{\numberline {2.1}Geometrie výpočetní sítě}{11}{subsection.2.1}%
\contentsline {section}{\numberline {3}Eulerovský krok ALE metod}{12}{section.3}%
\contentsline {subsection}{\numberline {3.1}Vyhlazování výpočetní sítě}{12}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Interpolace veličin mezi sítěmi}{12}{subsection.3.2}%
\contentsline {subsection}{\numberline {3.3}Rekonstrukce veličiny}{13}{subsection.3.3}%
\contentsline {subsubsection}{\numberline {3.3.1}Konstantní rekonstrukce}{13}{subsubsection.3.3.1}%
\contentsline {subsubsection}{\numberline {3.3.2}Lineární rekonstrukce}{13}{subsubsection.3.3.2}%
\contentsline {subsubsection}{\numberline {3.3.3}Lineární rekonstrukce s limiterem}{15}{subsubsection.3.3.3}%
\contentsline {subsubsection}{\numberline {3.3.4}Polynomiální rekonstrukce}{17}{subsubsection.3.3.4}%
\contentsline {subsubsection}{\numberline {3.3.5}THINC rekonstrukce}{18}{subsubsection.3.3.5}%
\contentsline {subsubsection}{\numberline {3.3.6}Rekonstrukce nespojitou funkcí}{20}{subsubsection.3.3.6}%
\contentsline {subsubsection}{\numberline {3.3.7}Rekonstrukce v okolí nespojitosti}{23}{subsubsection.3.3.7}%
\contentsline {subsubsection}{\numberline {3.3.8}Okrajové podmínky}{24}{subsubsection.3.3.8}%
\contentsline {subsection}{\numberline {3.4}Výpočet hodnot na nové síťce}{24}{subsection.3.4}%
\contentsline {subsection}{\numberline {3.5}Remap v ALE kódu}{25}{subsection.3.5}%
\contentsline {subsection}{\numberline {3.6}Multimateriálový remap}{25}{subsection.3.6}%
\contentsline {section}{\numberline {4}Lagrangeovský krok ALE metod}{27}{section.4}%
\contentsline {subsection}{\numberline {4.1}Fyzikální popis}{27}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Diskretizace prvního řádu}{28}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Diskretizace druhého řádu}{29}{subsection.4.3}%
\contentsline {subsection}{\numberline {4.4}Velikost časového kroku}{29}{subsection.4.4}%
\contentsline {subsection}{\numberline {4.5}Okrajové podmínky}{30}{subsection.4.5}%
\contentsline {subsection}{\numberline {4.6}Sledování kontaktu}{30}{subsection.4.6}%
\contentsline {subsection}{\numberline {4.7}Detekce komprese}{31}{subsection.4.7}%
\contentsline {section}{\numberline {5}Implementace}{32}{section.5}%
\contentsline {subsection}{\numberline {5.1}Použité knihovny}{32}{subsection.5.1}%
\contentsline {section}{\numberline {6}Numerické výsledky - remap}{33}{section.6}%
\contentsline {subsection}{\numberline {6.1}Hladký pohyb sítě}{33}{subsection.6.1}%
\contentsline {subsection}{\numberline {6.2}Kvadratický polynom a sinus}{34}{subsection.6.2}%
\contentsline {subsection}{\numberline {6.3}Nespojitá funkce}{36}{subsection.6.3}%
\contentsline {subsection}{\numberline {6.4}Nespojitá nelineární funkce}{38}{subsection.6.4}%
\contentsline {section}{\numberline {7}Numerické výsledky - ALE metody}{42}{section.7}%
\contentsline {subsection}{\numberline {7.1}Riemmanovy problémy}{42}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Shu-Osherův problém}{44}{subsection.7.2}%
