# import os
import glob
#
# for file in glob.iglob(".\\scripts\\*.py"):
#     print(file)
#     os.system("python " + file)
import os
import subprocess

for file_path in glob.iglob("./scripts/*.py"):
    print("Running: {}".format(file_path))

    # call script
    # subprocess.run(['python', file_path], cwd=os.path.abspath("./scripts"))
    subprocess.run(['python', os.path.abspath(file_path)], cwd=os.path.abspath("./scripts"))


print("Finished drawing all plots!")
