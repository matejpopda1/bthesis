import matplotlib.pyplot as plt
import json

## adds parent folder to path
import os
import sys
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, parentdir)

import general_settings


# json_paths_list = [
#     # "../../multimaterial/results/sin_sin_interface_20_nodes",
#     # "../../multimaterial/results/sin_sin_interface_40_nodes",
#     # "../../multimaterial/results/sin_sin_interface",
#     # "../../multimaterial/results/sin_sin_interface_160_nodes",
#     # "../../multimaterial/results/sin_sin_interface_320_nodes",
#
#
#     # "../../multimaterial/results/lin_lin_interface",
#     # "../../multimaterial/results/lin_lin_interface_with_one_sided_stencil",
#
#     # "../../multimaterial/results/quad_quad_interface",
#     # "../../multimaterial/results/quad_quad_interface_one_sided_stencil",
#     # "../../multimaterial/results/test",
#     # "../../singlematerial/results/lin_lin_jump"
#
#
#     # "../../multimaterial/results/sin_1st_degree",
#     # "../../multimaterial/results/sin_2nd_degree",
#     # "../../multimaterial/results/sin_4th_degree",
#
#
#
#
#
#     # "../../multimaterial/results/20nodes/const_rec_jump_fun",
#     # "../../multimaterial/results/20nodes/const_rec_lin_fun",
#     # "../../multimaterial/results/20nodes/lim_rec_jump",
#     # "../../multimaterial/results/20nodes/lim_rec_sin",
#     # "../../multimaterial/results/20nodes/lin_rec_jump",
#     # "../../multimaterial/results/20nodes/lin_rec_sin",
#     # "../../multimaterial/results/20nodes/multimat_jump",
#     # "../../multimaterial/results/20nodes/multimat_linlin",
#     # "../../multimaterial/results/20nodes/multimat_polypoly",
#     # "../../multimaterial/results/20nodes/multimat_sinsin",
#     # "../../multimaterial/results/20nodes/poly_rec_jump",
#     # "../../multimaterial/results/20nodes/poly_rec_sin",
#
#     # "../../multimaterial/results/20nodes/multimat_linlin_onesided",
#     # "../../multimaterial/results/20nodes/multimat_sinsin_onesided",
#
#
#     # "../../singlematerial/results/const_const_jump",
#     # "../../singlematerial/results/const_const_thinc5",
#     # "../../singlematerial/results/const_const_thinc15",
#     # "../../singlematerial/results/lin_lin_jump",
#     "../../singlematerial/results/lin_lin_thinc10",
#
# ]


json_path = "../../singlematerial/results/lin_lin_jump"


do_midpoints_unconnected = True
do_midpoints_connected = False
do_pointwise = True
do_analytic_function = True
do_midpoints_error = False

do_cell_borders = False


eps = 0.002

ignore_empty_cells = False  # should be False and its use case should be handled by saver.py



def color_based_on_mat(input):
    if input == "mat1":
        return general_settings.color_reconstruction_1
    elif input == "mat2":
        return general_settings.color_reconstruction_2
    else:
        return general_settings.color_reconstruction_mix


if __name__ == "__main__":

    current_output_dict = json.load(open(json_path + "/RawOutput.json"))
    current_input_dict = json.load(open(json_path + "/RawInput.json"))
    fig, ax = plt.subplots()


    if do_analytic_function:
        ax.plot(current_output_dict['analytic function'][0], current_output_dict['analytic function'][1], color=general_settings.color_original_function)

    if do_midpoints_unconnected:
        for i in current_output_dict['midpoints']:
            ax.scatter(i[0], i[1], color=color_based_on_mat(i[2]))

    if do_midpoints_connected:
        temp_xx = []
        temp_yy = []
        current_x = None
        for i in current_output_dict['midpoints']:
            if current_x is None or i[0] > current_x:  # Not exactly sure why this seems to work
                temp_xx.append(i[0])
                current_x = i[0]
                temp_yy.append(i[1])
        ax.plot(temp_xx, temp_yy, color=color_based_on_mat(i[2]))

    if do_pointwise:
        for cells in current_output_dict['simple']:
            if ignore_empty_cells and cells[1][0] < eps and cells[1][len(cells[1]) - 1] < eps:
                continue
            temp_xx = []
            temp_yy = []

            if ignore_empty_cells:
                for i in range(len(cells[1])):
                    if cells[1][i] > eps:
                        temp_xx.append(cells[0][i])
                        temp_yy.append(cells[1][i])
            else:
                temp_xx = cells[0]
                temp_yy = cells[1]
            ax.plot(temp_xx, temp_yy, color=color_based_on_mat(cells[2]))

    if do_cell_borders:  # TODO check that all inputs have the same amount of graphs? probably will be left as an user error
        for x in current_output_dict['nodes2']:
            ax.axvline(x, color=general_settings.color_cell_borders)



    if do_midpoints_error:
        for i in range(len(current_input_dict['midpoints'])):
            if abs(current_input_dict['midpoints'][i][0] - current_output_dict['midpoints'][i][0]) > eps:
                print("Different meshes, error might not make sense")
            ax.scatter(current_input_dict['midpoints'][i][0], abs(current_input_dict['midpoints'][i][1] - current_output_dict['midpoints'][i][1]), color='r')



    def get_script_name(include_extension):
        full_path = __file__
        file_name = full_path.rsplit("\\")[-1]
        if include_extension:
            return file_name
        else:
            return file_name.rsplit(".")[0]

    path_and_filename = "../results/" + get_script_name(False) + ".svg"
    plt.savefig(fname=path_and_filename, format="svg")
    print("Finished with the file " + get_script_name(True))

