


color_reconstruction_1 = "blue"
color_reconstruction_2 = "red"
color_reconstruction_mix = "orange"

color_original_function = "magenta"

color_cell_borders = "black"

colors_multiple_functions = ['g', 'b', 'c', 'm', 'y', 'r']

color_error = "red"
