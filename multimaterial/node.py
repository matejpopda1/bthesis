import random
import sys

import numpy as np


### this imports settings from the destination folder, we are abusing argv being global
try:
    sys.path.append(sys.argv[1])
    import options as settings
except IndexError:
    import settings

class Node:

    def __init__(self, position: float):
        """
        :param position: x-coordinate
        """
        self.position = position


    def __str__(self):
        return str(self.position)


def create_equidistant_nodes(number_of_nodes, starting_point=-1, ending_point=1):
    """
    Creates a grid of equidistant nodes
    :param ending_point:
    :param starting_point:
    :param number_of_nodes:
    :return: Grid of equidistant nodes
    """
    result = []
    step_length = (ending_point - starting_point) / (number_of_nodes - 1)
    for i in range(number_of_nodes):  # (nodes go from 0 to number_of_nodes - 1 )
        result.append(Node(step_length * i + starting_point))
    return result


def create_random_nodes(number_of_nodes, starting_point=-1, ending_point=1):
    """
    Creates a grid of nodes with random spacing
    :param number_of_nodes:
    :param starting_point:
    :param ending_point:
    """
    result = [0]
    total_length = 0
    for i in range(number_of_nodes - 1):
        x = random.randint(10, 50)
        total_length += x
        result.append(total_length)
    for i in range(number_of_nodes):
        result[i] = (result[i] / total_length) * (ending_point - starting_point) + starting_point
    for i in range(number_of_nodes):
        result[i] = Node(result[i])
    return result



# based on Second-Order Sign-Preserving Remapping on General Grids by Margolin and Shashkov
def create_nodes_for_cyclic_remapping(n, n_max, i_max, starting_point=-1, ending_point=1):
    result = []
    t_n = n/n_max

    def alpha(t):
        return np.sin(4*np.pi*t)/2

    def x(eta, t):
        return (1 - alpha(t))*eta + alpha(t)*eta**3

    for i in range(i_max - 1):
        eta_i = i / (i_max - 1)
        result.append(Node(x(eta_i, t_n) * (ending_point - starting_point) + starting_point))
    result.append(Node(ending_point))  # must be on a separate line, otherwise numerical inaccuracy messes up with the result

    return result

