from typing import List
import copy

import sys
### this imports settings from the destination folder, we are abusing argv being global
try:
    sys.path.append(sys.argv[1])
    import options as settings
except IndexError:
    import settings


class Cell:
    def __init__(self, volume: float, mid_point: float, value: float = None):
        self.volume = volume
        self.mid_point = mid_point
        self.average_value = value
        self.volume_fraction = []
        self.mat_in_cell = []  # 1 for first material, 2 for second material
        self.mat_volume = []
        self.mat_mid_point = []

        self.mat_average_value = []

        self.reconstruction_type = copy.deepcopy(settings.global_reconstruction_type)
        self.mat_derivative: List[List[float]] = [[], [0], [0]]  # list of derivatives, starting at the first derivative
        self.conservation_constants: List[float] = [[], [0, 0], [0 , 0] ]  # list of conservation constant starting at k_0, -> first 2 values are always supposed to be 0

    def get_materials(self):
        if self.mat_in_cell[0] != 0:
            return [self.mat_in_cell[0]]
        result = []
        for i, j in enumerate(self.mat_in_cell):
            if j == 1:
                result.append(i)
        return result

    def get_mat_center(self, material):
        if self.mat_in_cell[material] != 1:
            raise RuntimeError("Material not in cell")
        return self.mat_mid_point[material]

    def get_mat_value(self, material):
        if self.mat_in_cell[material] != 1:
            raise RuntimeError("Material not in cell")
        return self.mat_average_value[material]

    def get_mat_volume(self, material):
        if self.mat_in_cell[material] != 1:
            raise RuntimeError("Material not in cell")
        return self.mat_volume[material]

    def has_mat(self, mat):
        if mat >= len(self.mat_in_cell):
            return 0
        return self.mat_in_cell[mat] == 1

    def get_mat_conservation_constants(self, mat=None):
        if mat is None:
            return self.conservation_constants[self.mat_in_cell[0]]
        else:
            return self.conservation_constants[mat]

    def set_mat_conservation_constants(self, constants_list, mat=None):
        if mat is None:
            self.conservation_constants[self.mat_in_cell[0]] = constants_list
        else:
            self.conservation_constants[mat] = constants_list