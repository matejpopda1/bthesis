import os

all_paths = [
    "results/const-const",
    # "results/lin_lin_interface",
    "results/quad_quad_interface",
    "results/quad_quad_interface40",
    "results/quad_quad_interface160",
    # "results/sin_sin_interface_20_nodes",
    # "results/sin_sin_interface_40_nodes",
    # "results/sin_sin_interface",
    # "results/sin_sin_interface_160_nodes",
    # "results/sin_sin_interface_320_nodes",
    # "results/lin_lin_interface_with_one_sided_stencil",
    # "results/quad_quad_interface_one_sided_stencil",
    # "results/sin_1st_degree",
    # "results/sin_2nd_degree",
    # "results/sin_4th_degree",

    # "results/20nodes/const_rec_jump_fun",
    # "results/20nodes/const_rec_lin_fun",
    # "results/20nodes/lim_rec_jump",
    # "results/20nodes/lim_rec_sin",
    # "results/20nodes/lin_rec_jump",
    # "results/20nodes/lin_rec_sin",
    # "results/20nodes/multimat_jump",
    # "results/20nodes/multimat_linlin",
    # "results/20nodes/multimat_polypoly",
    # "results/20nodes/multimat_sinsin",
    # "results/20nodes/poly_rec_jump",
    # "results/20nodes/poly_rec_sin",

    "results/20nodes/multimat_linlin_onesided",
    "results/20nodes/multimat_sinsin_onesided",
]

if __name__ == "__main__":
    for path in all_paths:
        print("main2.py " + path)
        os.system("python main2.py " + path)

    print("done")

