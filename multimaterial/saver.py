import sys
import drawing
import settings
from multimaterial import mesh
import node
from multimaterial.writer import write_out_results_and_settings


def results_from_options_at_path(options_path):
    """
    options_path: directory containing options.py (doesnt end with /)
    draws the final graph and saves some stats in the same folder
    """
    sys.path.append(options_path)
    import options as s
    settings.input_function = s.input_function
    settings.do_one_sided_reconstruction = s.do_one_sided_reconstruction

    temp_mesh = mesh.function_to_mesh(s.input_function, s.number_of_nodes, s.starting_point, s.ending_point)

    drawing.save(s.plotting_options, temp_mesh, options_path, s.input_function, "/RawInput.json", "start")

    for i in range(s.how_many_iterations + 1):
        temp_mesh.remap(node.create_nodes_for_cyclic_remapping(i, s.how_many_iterations, s.number_of_nodes))

    drawing.save(s.plotting_options, temp_mesh, options_path, s.input_function, "/RawOutput.json")
    write_out_results_and_settings(temp_mesh, s.input_function, options_path)

