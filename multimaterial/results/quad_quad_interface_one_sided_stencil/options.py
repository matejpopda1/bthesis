from multimaterial import test_functions


# domain setting
starting_point = -1
ending_point = 1

# mesh settings
number_of_nodes = 60

# how many times to remap
how_many_iterations = 160

# type of reconstruction in non-marked cell
type_of_reconstruction = "P"
# type_of_reconstruction = "P+BJ"

# degree of polynomial in the reconstruction
max_polynomial_degree = 2


# input function
input_function = test_functions.quad_quad_interface
do_one_sided_reconstruction = True


# graphing settings
number_of_samples_for_graph = 200



plotting_options = [
    "midpoints",
    # "steps",
    # "slopes",
    # "nodes",
    # "nodes2",
    "simple"
]

#########################################################################

draw_after_how_many_iterations = how_many_iterations

global_reconstruction_type = [type_of_reconstruction, max_polynomial_degree]



# ghost nodes settings
overshoot_as_percentage_of_total_computational_domain = 20

import random
# set seed for debugging
random.seed(51)