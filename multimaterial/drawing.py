import mesh
import matplotlib.pyplot as plt
import numpy as np
import node
import json


import sys
### this imports settings from the destination folder, we are abusing argv being global
try:
    sys.path.append(sys.argv[1])
    import options as settings
except IndexError:
    import settings

def plot_midpoints(input_mesh: mesh.Mesh, ax: plt.Axes, output_raw=None):
    for i in input_mesh.cells:
        if i.mat_in_cell[0] == 1:
            ax.scatter(i.mid_point,  i.average_value, color="blue")

            if output_raw is not None:
                output_raw['midpoints'].append([i.mid_point,  i.average_value, 'mat1'])

        elif i.mat_in_cell[0] == 2:
            ax.scatter(i.mid_point, i.average_value, color="green")
            if output_raw is not None:
                output_raw['midpoints'].append([i.mid_point,  i.average_value, 'mat2'])

        elif i.mat_in_cell[0] == 0:
            ax.scatter(i.mid_point, i.average_value, color="red", marker="+", label="Střední hodnota smíšené buňky")
            ax.scatter(i.mat_mid_point[1], i.mat_average_value[1], color="blue", label="Střední hodnota prvního materiálu")
            ax.scatter(i.mat_mid_point[2], i.mat_average_value[2], color="green", label="Střední hodnota druhého materiálu")
            if output_raw is not None:
                output_raw['midpoints'].append([i.mat_mid_point[1], i.mat_average_value[1], 'mat1'])
                output_raw['midpoints'].append([i.mid_point,  i.average_value, 'mixed'])
                output_raw['midpoints'].append([i.mat_mid_point[2], i.mat_average_value[2], 'mat2'])



def plot_nodes(input_mesh: mesh.Mesh, ax: plt.Axes):
    result = []
    for i in range(len(input_mesh.nodes)):
        result.append(input_mesh.nodes[i].position)
    ax.scatter(result, np.zeros(len(result)), color="black")

def plot_cell_borders(input_mesh: mesh.Mesh, ax: plt.Axes, output_raw=None):
    for i in range(len(input_mesh.nodes)):
        ax.axvline(input_mesh.nodes[i].position, color="black")
        if output_raw is not None:
            output_raw['nodes2'].append(input_mesh.nodes[i].position)

def plot_pointwise(input_mesh: mesh.Mesh, ax: plt.Axes, output_raw=None):
    input_mesh.reconstruction()
    colors = ["black", "blue", "green"]
    for i in range(len(input_mesh.cells)):
        for materials in [1, 2]:
            if not input_mesh.cells[i].has_mat(materials):
                continue
            number_of_samples_in_cell = settings.number_of_samples_for_graph // settings.number_of_nodes
            if len(input_mesh.cells[i].get_materials()) == 2:
                length = min(abs((input_mesh.cells[i].get_mat_center(materials))-input_mesh.nodes[i].position), abs((input_mesh.cells[i].get_mat_center(materials))-input_mesh.nodes[i+1].position)) - 0.0000000001
                xx = np.linspace(input_mesh.cells[i].get_mat_center(materials) - length, input_mesh.cells[i].get_mat_center(materials) + length, number_of_samples_in_cell)
            else:
                xx = np.linspace(input_mesh.nodes[i].position + 0.0000000001, input_mesh.nodes[i + 1].position - 0.0000000001, number_of_samples_in_cell)
            yy = np.zeros(number_of_samples_in_cell)
            for j in range(number_of_samples_in_cell):
                yy[j] = input_mesh.mat_value_at_x(xx[j], materials)

            label = ""
            if materials == 1:
                label = "Rekonstrukce prvního materiálu"
            elif materials ==2:
                label = "Rekonstrukce druhého materiálu"


            ax.plot(xx, yy, c=colors[materials], label=label)
            # ax.scatter(xx, yy, c=colors[materials], marker=".")
            if output_raw is not None:
                output_raw["simple"].append([xx.tolist(),yy.tolist(),"mat"+str(materials), "cell" + str(i)])



# setup
all_draw_options = {
    "midpoints": plot_midpoints,
    # "steps": plot_steps,
    # "slopes": plot_slopes,
    "nodes": plot_nodes,
    "nodes2": plot_cell_borders,
    "simple": plot_pointwise,
}



def draw(options: list, input_mesh: mesh.Mesh, function=None):
    """
    :param options: List of any combination of the following options "midpoints", "steps", "slopes", "nodes", "simple"
    :param input_mesh: mesh that is getting drawn
    :param function: optional parameter for drawing the analytic function
    :return: void
    """
    graph_domain = np.linspace(settings.starting_point, settings.ending_point, settings.number_of_samples_for_graph)
    graph_range = np.zeros(settings.number_of_samples_for_graph)


    fig, ax = plt.subplots()

    # selects only values that are both in possible_options and options
    possible_options = [plot_midpoints]
    for i in range(len(options)):
        possible_options.append(all_draw_options.get(options[i]))

    if function is not None:
        for i in range(settings.number_of_samples_for_graph):
            graph_range[i] = function(graph_domain[i])
        ax.plot(graph_domain, graph_range, color="red")

    for i in possible_options:
        i(input_mesh, ax)

    plt.show()



def save(options: list, input_mesh: mesh.Mesh, path, function=None, file_name="RawData.json", image_name="result"):
    """
    :param options: List of any combination of the following options "midpoints", "steps", "slopes", "nodes", "simple"
    :param input_mesh: mesh that is getting drawn
    :param function: optional parameter for drawing the analytic function
    :return: void
    """

    graph_domain = np.linspace(settings.starting_point, settings.ending_point, settings.number_of_samples_for_graph)
    graph_range = np.zeros(settings.number_of_samples_for_graph)



    fig, ax = plt.subplots()

    points_for_drawing = {}

    # selects only values that are both in possible_options and options
    possible_options = [plot_midpoints]
    for i in range(len(options)):
        possible_options.append(all_draw_options.get(options[i]))
        points_for_drawing[options[i]] = []


    if function is not None:
        for i in range(settings.number_of_samples_for_graph):
            graph_range[i] = function(graph_domain[i])
        ax.plot(graph_domain, graph_range, color="pink", label="Původní funkce")
        points_for_drawing["analytic function"] = [graph_domain.tolist(), graph_range.tolist()]

    for i in possible_options:
        i(input_mesh, ax, points_for_drawing)

    # print(points_for_drawing)
    json.dump(points_for_drawing, open(path + file_name, 'w'))

    handles, labels = plt.gca().get_legend_handles_labels()
    by_label = dict(zip(labels, handles))
    plt.legend(by_label.values(), by_label.keys(), loc=0)


    plt.savefig(path + "/" + image_name + ".svg", format='svg', dpi=1200)


def draw_from_global_settings(input_mesh: mesh.Mesh):
    for i in range(settings.how_many_iterations):
        # input_mesh.remap()
        input_mesh.remap(node.create_nodes_for_cyclic_remapping(i, settings.how_many_iterations, settings.number_of_nodes))
        # print(f"For individual materials we have {input_mesh.calculate_value_mat(1)} for mat1 and {input_mesh.calculate_value_mat(2)} for mat2")

        if i % settings.draw_after_how_many_iterations == 0:
            draw(settings.plotting_options, input_mesh, settings.input_function)
    if (settings.how_many_iterations - 1) % settings.draw_after_how_many_iterations != 0:
        draw(settings.plotting_options, input_mesh, settings.input_function)
