import mesh
import test_functions
import node
import drawing
import settings
import writer
import saver
import sys



if __name__ == "__main__":

    saver.results_from_options_at_path(sys.argv[1])

    """
    Remaining code is mostly for debugging, when running disable the code above
    Also most of the code can have undefined behavior where it takes some values from settings.py and some from provided options.py
    """

    # saver.results_from_options_at_path("results/sin_sin_interface_40_nodes")
    # saver.results_from_options_at_path("results/sin_sin_interface")
    # saver.results_from_options_at_path("results/lin_lin_interface")
    # saver.results_from_options_at_path("results/lin_lin_interface_with_one_sided_stencil")
    # saver.results_from_options_at_path("results/test")
    # saver.results_from_options_at_path("results/quad_quad_interface_one_sided_stencil")

    # writer.writer_wrapper()

    # x = mesh.function_to_mesh(settings.input_function, settings.number_of_nodes, settings.starting_point, settings.ending_point)
    # print(f"The value at the start is {x.calculate_value()}")
    # print(f"For individual materials we have {x.calculate_value_mat(1)} for mat1 and {x.calculate_value_mat(2)} for mat2")
    # drawing.draw_from_global_settings(x)
    # print(f"The value at the end is {x.calculate_value()}")
    # print(f"For individual materials we have {x.calculate_value_mat(1)} for mat1 and {x.calculate_value_mat(2)} for mat2")
    # print(x.max_error(), x.l1_error(), x.l2_error(), x.l_inf_error())
    exit()
