import matplotlib.pyplot as plt
import drawing
import mesh
import pandas as pd
import node
import test_functions


import sys
### this imports settings from the destination folder, we are abusing argv being global
try:
    sys.path.append(sys.argv[1])
    import options as s
except IndexError:
    import settings as s

def write_out_results_and_settings(input_mesh: mesh.Mesh, input_function, path = None):
    result = {}
    result["Number_of_iterations"] = s.how_many_iterations
    result["Start_of_computational_domain"] = str(s.starting_point)
    result["End_of_computational_domain"] = str(s.ending_point)
    result["Number_of_nodes"] = s.number_of_nodes
    result["Original_function"] = input_function.__name__
    result["Original_function_desc"] = input_function.__doc__
    result["Total mass change"] = input_mesh.calculate_value() - input_mesh.starting_total_mass
    result["Used global type of reconstruction"] = s.global_reconstruction_type[0]
    result["Maximum_degree_of_polynomial_in_reconstruction"] = s.global_reconstruction_type[1]
    result["L1_error"] = input_mesh.l1_error()
    result["L2_error"] = input_mesh.l2_error()
    result["L_inf_error"] = input_mesh.l_inf_error()
    result["max_error"] = input_mesh.max_error()
    result["mat1_L1_error"] = input_mesh.l1_error_mat(1)
    result["mat1_L2_error"] = input_mesh.l2_error_mat(1)
    result["mat1_L_inf_error"] = input_mesh.l_inf_error_mat(1)
    result["mat1_max_error"] = input_mesh.max_error_mat(1)
    if len(input_function.list_of_materials) >= 2:
        result["mat2_L1_error"] = input_mesh.l1_error_mat(2)
        result["mat2_L2_error"] = input_mesh.l2_error_mat(2)
        result["mat2_L_inf_error"] = input_mesh.l_inf_error_mat(2)
        result["mat2_max_error"] = input_mesh.max_error_mat(2)

    if path is None:
        try:
            df = pd.read_excel("results.xlsx")
            df = df.append(result, ignore_index=True)
            print("With {nodes} nodes we achieve errors: {l1} for the L_1 error, {l2} for the L_2 error, "
                  "{l_inf} for the L_inf and {max} for the max error".format(nodes=result["Number_of_nodes"],
                                                                             l1=result["L1_error"],
                                                                             l2=result["L2_error"],
                                                                             l_inf=result["L_inf_error"],
                                                                             max=result["max_error"]))
        except FileNotFoundError:
            df = pd.DataFrame(result, index=[0])
        df.to_excel("results.xlsx", index=False)
    else:
        df = pd.DataFrame(result, index=[0])
        df.to_json(path + "/result.json")



def calculate_multiple_results(list_of_how_many_nodes, draw_at_the_end=True):
    for j in list_of_how_many_nodes:
        s.number_of_nodes = j
        temp_mesh = mesh.function_to_mesh(s.input_function, s.number_of_nodes, s.starting_point, s.ending_point)
        for i in range(s.how_many_iterations):
            # temp_mesh.remap(node.create_random_nodes(s.number_of_nodes))
            temp_mesh.remap(node.create_nodes_for_cyclic_remapping(i, s.how_many_iterations, s.number_of_nodes))
        if draw_at_the_end:
            drawing.draw(s.plotting_options, temp_mesh, s.input_function)
        write_out_results_and_settings(temp_mesh, s.input_function)


def writer_wrapper(draw_at_the_end=True):
    ## TODO: this sometimes crashes? not sure why, but data can be gotten out of this regardless
    functions_to_draw = [
        # test_functions.quad_quad_interface,
        # test_functions.quad_no_interface,
        # test_functions.lin_with_2jump,
        # test_functions.lin_no_interface,
        test_functions.sin_sin_interface
    ]

    iterations_to_try = [500]

    reconstructions_to_try = [
        ["P", 0],
        ["P", 1],
        ["P+BJ", 1],
        ["P", 2]
    ]

    how_many_nodes = [20, 40, 80, 160, 320]
    # how_many_nodes = [40, 80]

    for i in iterations_to_try:
        s.how_many_iterations = i
        for f in functions_to_draw:
            s.input_function = f
            for r in reconstructions_to_try:
                s.global_reconstruction_type = r
                calculate_multiple_results(how_many_nodes, draw_at_the_end=draw_at_the_end)


def save_figure(options: list, input_mesh: mesh.Mesh, name: str = "figure"):
    fig, ax = plt.subplots()

    # selects only values that are both in possible_options and options
    possible_options = []
    for i in range(len(options)):
        possible_options.append(drawing.all_draw_options.get(options[i]))
    for i in possible_options:
        i(input_mesh, ax)

    plt.savefig(name, format='svg', dpi=1200)
