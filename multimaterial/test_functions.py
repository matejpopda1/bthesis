import math

import numpy as np


# Definitions of decorators
def list_of_interfaces(interfaces):
    """
    Adds a list of interfaces to the function
    """
    if not isinstance(interfaces, list):
        raise RuntimeError("Provided interfaces are not a list")

    def wrapper(function):
        function.list_of_interfaces = interfaces
        return function

    return wrapper


# Definitions of decorators
def list_of_materials(materials):
    """
    Adds a sequence of materials, going from left to right
    """
    if not isinstance(materials, list):
        raise RuntimeError("Provided materials are not a list")

    def wrapper(function):
        function.list_of_materials = materials
        return function

    return wrapper


@list_of_materials([1])
@list_of_interfaces([])
def const_no_interface(x):
    return 1


@list_of_materials([1])
@list_of_interfaces([])
def lin_no_interface(x):
    return x + 1


@list_of_materials([1])
@list_of_interfaces([])
def sin_no_interface(x):
    return math.sin(x * 2 * math.pi) + 2.5


@list_of_materials([1, 2])
@list_of_interfaces([])
def quad_no_interface(x):
    return x * x + 1


@list_of_materials([1, 2])
@list_of_interfaces([0])
def const(x):
    return 1


@list_of_materials([1, 2])
@list_of_interfaces([0.00001])
def quad_quad_interface(x):
    if x > 0:
        return x * x + 1 - x
    else:
        return - x * x + 2 - x


@list_of_materials([1, 2])
@list_of_interfaces([0.00001])
def sin_sin_interface(x):
    if x > 0:
        return np.sin(2*x + 1)
    else:
        return np.sin(6*x + 5) + 1



@list_of_materials([1, 2])
@list_of_interfaces([0])
def const_with_jump(x):
    if x > 0:
        return 1
    else:
        return 2


@list_of_materials([1, 2])
@list_of_interfaces([-0.5, 0.5])
def const_with_2jump(x):
    if abs(x) > 0.5:
        return 1
    else:
        return 2


@list_of_materials([1, 2])
@list_of_interfaces([-0.5, 0.5])
def lin_with_2jump(x):
    if abs(x) > 0.5:
        return abs(x)
    else:
        return 1.5 + 0.5 * x


@list_of_materials([1, 2])
@list_of_interfaces([0])
def lin_lin_interface(x):
    if x < 0:
        return -2*x + 1
    else:
        return x+2


@list_of_materials([1, 2])
@list_of_interfaces([0.00001])
def test(x):
    x_c = -0.03332833333333333
    if x < 0:
        return 2 + (x - x_c)*4 + 7 * ((x - x_c)**2 - 0.0003704814898148146)
    else:
        return 1


@list_of_materials([1])
@list_of_interfaces([])
def sine(x):
    return np.sin(x*2) + 2

@list_of_materials([1])
@list_of_interfaces([])
def one_mat_jump(x):
    if x > 0:
        return 2
    return 1