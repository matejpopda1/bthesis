import copy
import functools
from typing import List

import numpy as np
import scipy.linalg

import node
import cell
import test_functions
import scipy.integrate



import sys
### this imports settings from the destination folder, we are abusing argv being global
try:
    sys.path.append(sys.argv[1])
    import options as settings
except IndexError:
    import settings


def integrate_function(function, a, b):
    return scipy.integrate.quad(function, a, b, points=function.list_of_interfaces)[0]





def create_cells(nodes, function):
    resulting_list = []
    max_materials_in_cell = len(function.list_of_materials) + 1
    for i in range(len(nodes) - 1):
        volume = nodes[i + 1].position - nodes[i].position
        midpoint = (nodes[i].position + nodes[i + 1].position) / 2
        temp_cell = cell.Cell(volume, midpoint,
                              integrate_function(function, nodes[i].position, nodes[i + 1].position) / volume)

        # what materials are in cell
        temp_cell.mat_in_cell = [0]*max_materials_in_cell
        temp_cell.volume_fraction = [0] * max_materials_in_cell
        temp_cell.mat_volume = [0] * max_materials_in_cell
        temp_cell.mat_mid_point = [0] * max_materials_in_cell
        temp_cell.mat_average_value = [0] * max_materials_in_cell

        temp_cell.mat_mid_point[0] = temp_cell.mid_point
        temp_cell.mat_volume[0] = volume
        temp_cell.volume_fraction[0] = 1
        temp_cell.mat_average_value[0] = temp_cell.average_value

        left_node = nodes[i].position
        right_node = nodes[i + 1].position
        what_index_mat_on_left_node = sum([1 for h in function.list_of_interfaces if h < left_node])
        first_material: int = function.list_of_materials[what_index_mat_on_left_node % (len(function.list_of_materials))]
        second_material: int = function.list_of_materials[(what_index_mat_on_left_node + 1) % (len(function.list_of_materials))]
        interfaces_in_cell = [k for k in function.list_of_interfaces if left_node < k < right_node]
        if len(interfaces_in_cell) == 1:
            # case when 2 mats
            temp_cell.mat_in_cell[0] = 0
            temp_cell.mat_in_cell[first_material] = 1
            temp_cell.mat_in_cell[second_material] = 1

            temp_cell.mat_mid_point[first_material] = (interfaces_in_cell[0] + left_node)/2
            temp_cell.mat_volume[first_material] = interfaces_in_cell[0] - left_node
            temp_cell.volume_fraction[first_material] = temp_cell.mat_volume[first_material]/volume

            temp_cell.mat_average_value[first_material] = integrate_function(function, nodes[i].position, interfaces_in_cell[0]) / temp_cell.mat_volume[first_material]

            temp_cell.mat_mid_point[second_material] = (interfaces_in_cell[0] + right_node)/2
            temp_cell.mat_volume[second_material] = right_node - interfaces_in_cell[0]
            temp_cell.volume_fraction[second_material] = temp_cell.mat_volume[second_material]/volume

            temp_cell.mat_average_value[second_material] = integrate_function(function, interfaces_in_cell[0], nodes[i+1].position) / temp_cell.mat_volume[second_material]

        elif len(interfaces_in_cell) == 0:
            temp_cell.mat_in_cell[first_material] = 1
            temp_cell.mat_in_cell[0] = first_material
            temp_cell.mat_average_value[first_material] = temp_cell.average_value
            temp_cell.mat_mid_point[first_material] = temp_cell.mid_point
            temp_cell.mat_volume[first_material] = volume
            temp_cell.volume_fraction[first_material] = 1

        else:
            raise RuntimeError("Tried having 2 interfaces in one cell")
        resulting_list.append(temp_cell)
    return resulting_list


def function_to_mesh_from_node_list(function, list_of_nodes):
    """
    Takes a function from interval <starting_point,ending_point> and turns it into a mesh
    :param list_of_nodes:
    :param function: function that is getting turned into a mesh
    :return: resulting mesh
    """
    mesh = Mesh()

    mesh.list_of_materials = function.list_of_materials
    mesh.list_of_interfaces = function.list_of_interfaces

    # creating nodes
    mesh.nodes = list_of_nodes

    # creating cells
    mesh.cells = create_cells(mesh.nodes, function)
    # creating ghost cells
    overshoot = (settings.ending_point - settings.starting_point) * (
                settings.overshoot_as_percentage_of_total_computational_domain / 100)
    temp_ghost_nodes_l = node.create_equidistant_nodes((settings.max_polynomial_degree // 2) + 2,
                                                       mesh.nodes[0].position - overshoot, mesh.nodes[0].position)
    temp_ghost_nodes_r = node.create_equidistant_nodes((settings.max_polynomial_degree // 2) + 2,
                                                       mesh.nodes[len(mesh.nodes) - 1].position,
                                                       mesh.nodes[len(mesh.nodes) - 1].position + overshoot)
    mesh.ghost_cells = create_cells(temp_ghost_nodes_r, function)
    mesh.ghost_cells.extend(create_cells(temp_ghost_nodes_l, function))

    mesh.ghost_nodes = temp_ghost_nodes_r[1:] + temp_ghost_nodes_l[:-1]

    mesh.starting_total_mass = mesh.calculate_value()

    return mesh


def function_to_mesh(function, number_of_nodes: int, starting_point: float,
                     ending_point: float):
    """
    Takes a function from interval <starting_point,ending_point> and turns it into a mesh
    :param function: function that is getting turned into a mesh
    :param number_of_nodes: number of nodes in the resulting mesh
    :param starting_point: where the sampling of function starts
    :param ending_point: where the sampling of function end
    :return: resulting mesh
    """
    if ending_point <= starting_point:
        print("Error : Starting point bigger or equal to ending point")
        exit()

    node.create_equidistant_nodes(number_of_nodes, starting_point, ending_point)

    return function_to_mesh_from_node_list(function, node.create_equidistant_nodes(number_of_nodes, starting_point, ending_point))





class Mesh:
    def __init__(self):
        self.nodes: List[node.Node] = []  # a list of nodes
        self.cells: List[cell.Cell] = []  # a list of cells
        self.ghost_cells: List[cell.Cell] = []  # a list of cells outside of computational domain
        self.ghost_nodes: List[
            node.Node] = []  # a list of nodes outside of the computational domain, first there are are right nodes, then left
        self.list_of_materials = []
        self.list_of_interfaces = []
        self.starting_total_mass = None

    def integrate(self, function, a, b):
        if a > b:
            temp = a
            a = b
            b = temp
        offset = (b - a)/5  # we would rather include more than less points
        points = [x for x in ([x.position for x in self.nodes] + self.list_of_interfaces) if a - offset < x < b + offset]
        return scipy.integrate.quad(function, a, b, points=points)[0]

    def inside_what_cell(self, x: float):
        """
        Binary search
        :return: index of the cell x is in
        """
        left = 0
        right = len(self.nodes) - 1
        while left <= right:
            mid = (right + left) // 2
            # If x is greater, ignore left half
            if self.nodes[mid + 1].position < x:
                left = mid + 1
            # If x is smaller, ignore right half
            elif self.nodes[mid].position > x:
                right = mid - 1
            # means x is present at mid
            else:
                return mid
        raise RuntimeWarning(
            "Exception " + str(x) + " is outside of domain")  # could add handling but it shouldn't be needed

    def mat_value_at_x(self, x, material):
        if x < settings.starting_point or x > settings.ending_point:
            raise RuntimeError("Outside of computational domain: " + str(x))
        index_of_current_cell = self.inside_what_cell(x)
        current_cell = self.cells[index_of_current_cell]
        result = 0
        if current_cell.mat_in_cell[0] != 0:
            if current_cell.mat_in_cell[0] != material:  # returns 0 if material is not in cell
                return 0
            h = x - current_cell.mid_point
            mat_derivative = current_cell.mat_derivative
            list_to_iterate = [current_cell.get_mat_value(material)] + current_cell.mat_derivative[material]
            for i, derivative in enumerate(list_to_iterate):
                result += (pow(h, i) - current_cell.get_mat_conservation_constants(material)[i]) * derivative
        else:  # Mixed cells case, at most there will be reconstruction of first degree
            materials_in_cell = current_cell.get_materials()
            mat1 = materials_in_cell[0]
            mat2 = materials_in_cell[1]
            if current_cell.mat_mid_point[mat1] >= current_cell.mat_mid_point[mat2]:
                left_mat = mat2
            else:
                left_mat = mat1
            interface = [x for x in self.list_of_interfaces if (self.nodes[index_of_current_cell].position < x < self.nodes[index_of_current_cell+1].position)][0]

            if (x <= interface and left_mat == material) or (x > interface and left_mat != material):
                list_to_iterate = [current_cell.get_mat_value(material)] + current_cell.mat_derivative[material]
                h = x - current_cell.get_mat_center(material)
                for i, derivative in enumerate(list_to_iterate):
                    result += (pow(h, i) - current_cell.get_mat_conservation_constants(material)[i]) * derivative

        return result

    def remap(self, list_of_new_nodes: List[node.Node]):
        temp_cells = []
        max_materials_in_cell = len(self.list_of_materials) + 1
        self.reconstruction()
        # creating cells
        for i in range(len(list_of_new_nodes) - 1):
            volume = list_of_new_nodes[i+1].position - list_of_new_nodes[i].position
            mid_point = (list_of_new_nodes[i].position + list_of_new_nodes[i+1].position)/2
            temp_cell = cell.Cell(volume, mid_point)
            temp_cells.append(temp_cell)

            # what materials are in cell
            temp_cell.mat_in_cell = [0]*max_materials_in_cell
            temp_cell.volume_fraction = [0] * max_materials_in_cell
            temp_cell.mat_volume = [0] * max_materials_in_cell
            temp_cell.mat_mid_point = [0] * max_materials_in_cell
            temp_cell.mat_average_value = [0] * max_materials_in_cell

            temp_cell.reconstruction_type = settings.global_reconstruction_type

            temp_cell.mat_mid_point[0] = temp_cell.mid_point
            temp_cell.mat_volume[0] = volume
            temp_cell.volume_fraction[0] = 1

            left_node = list_of_new_nodes[i].position
            right_node = list_of_new_nodes[i + 1].position
            what_index_mat_on_left_node = sum([1 for h in self.list_of_interfaces if h < left_node])
            first_material: int = self.list_of_materials[what_index_mat_on_left_node % (len(self.list_of_materials))]
            second_material: int = self.list_of_materials[(what_index_mat_on_left_node + 1) % (len(self.list_of_materials))]
            interfaces_in_cell = [k for k in self.list_of_interfaces if left_node < k < right_node]


            if len(interfaces_in_cell) == 1:
                temp_cell.mat_in_cell[0] = 0
                temp_cell.mat_in_cell[first_material] = 1
                temp_cell.mat_in_cell[second_material] = 1

                temp_cell.mat_mid_point[first_material] = (interfaces_in_cell[0] + left_node)/2
                temp_cell.mat_volume[first_material] = interfaces_in_cell[0] - left_node
                temp_cell.volume_fraction[first_material] = temp_cell.mat_volume[first_material]/volume

                def mat1_wrapper(x):
                    return self.mat_value_at_x(x, first_material)

                temp_cell.mat_average_value[first_material] = self.integrate(mat1_wrapper, left_node, interfaces_in_cell[0]) / temp_cell.mat_volume[first_material]

                def mat2_wrapper(x):
                    return self.mat_value_at_x(x, second_material)
                temp_cell.mat_mid_point[second_material] = (interfaces_in_cell[0] + right_node)/2
                temp_cell.mat_volume[second_material] = right_node - interfaces_in_cell[0]
                temp_cell.volume_fraction[second_material] = temp_cell.mat_volume[second_material]/volume

                temp_cell.mat_average_value[second_material] = self.integrate(mat2_wrapper, interfaces_in_cell[0], right_node) / temp_cell.mat_volume[second_material]

                temp_cell.mat_average_value[0] = temp_cell.volume_fraction[first_material] * temp_cell.mat_average_value[first_material] + temp_cell.volume_fraction[second_material] * temp_cell.mat_average_value[second_material]

            elif len(interfaces_in_cell) == 0:
                temp_cell.mat_in_cell[first_material] = 1
                temp_cell.mat_in_cell[0] = first_material

                def mat1_wrapper(x):
                    return self.mat_value_at_x(x, first_material)

                temp_cell.mat_average_value[first_material] = self.integrate(mat1_wrapper, list_of_new_nodes[i].position, list_of_new_nodes[i+1].position) / volume
                temp_cell.mat_average_value[0] = temp_cell.mat_average_value[first_material]
                temp_cell.mat_mid_point[first_material] = mid_point
                temp_cell.mat_volume[first_material] = volume
                temp_cell.volume_fraction[first_material] = 1

            else:
                raise RuntimeError("Error in remapping, multiple interfaces in a cell")

            temp_cell.average_value = temp_cell.mat_average_value[0]


        for i in temp_cells:
            i.reconstruction_type = copy.deepcopy(settings.global_reconstruction_type)  # TODO: replace hard coded value with some sort of detector?
        self.nodes = list_of_new_nodes
        self.cells = temp_cells



    def material_reconstruction(self):
        interfaces = []
        for i, cells in enumerate(self.cells):
            if cells.mat_in_cell[0] != 0:
                continue

            materials_in_cell = cells.get_materials()
            mat1 = materials_in_cell[0]
            mat2 = materials_in_cell[1]
            if cells.mat_mid_point[mat1] >= cells.mat_mid_point[mat2]:
                left_mat = mat2
            else:
                left_mat = mat1
            length = self.nodes[i+1].position - self.nodes[i].position
            interface_position = self.nodes[i].position + length * cells.volume_fraction[left_mat]
            interfaces.append(interface_position)
        self.list_of_interfaces = interfaces
        # print(str.format('{0:.35f}', interfaces[0]))

    def designate_cells_for_non_global_reconstruction(self):  # TODO handling for 2 interfaces next to each other
        self.material_reconstruction()
        max_degree = settings.max_polynomial_degree
        for interface in self.list_of_interfaces:
            cell_index = self.inside_what_cell(interface)
            if settings.do_one_sided_reconstruction:
                interface_reconstruction_degree = max_degree
            else:
                interface_reconstruction_degree = 0

            self.cells[cell_index].reconstruction_type = ["mixed", interface_reconstruction_degree]
            for i, j in enumerate(range(max_degree - 1, 0, -1)):
                if i == 0:
                    continue
                # if i == 1:
                #     self.cells[cell_index + 1].reconstruction_type = ["P+BJ", 1]
                #     self.cells[cell_index - 1].reconstruction_type = ["P+BJ", 1]
                #     continue
                if i >= 1 and settings.do_one_sided_reconstruction is False:
                    self.cells[cell_index + i].reconstruction_type[1] = max(min(self.cells[cell_index + i].reconstruction_type[1], max_degree - j), 1)
                    self.cells[cell_index - i].reconstruction_type[1] = max(min(self.cells[cell_index - i].reconstruction_type[1], max_degree - j), 1)




    def reconstruction(self):
        """
        Handles calculating all the parameters for reconstruction, ie. derivatives
        """
        self.designate_cells_for_non_global_reconstruction()
        possible_reconstruction_types = ["P", "P+BJ", "mixed"]

        joined_cells = self.cells + self.ghost_cells  # only used for the polynomial reconstruction

        for i, current_cell in enumerate(self.cells):
            if current_cell.reconstruction_type[0] not in possible_reconstruction_types:
                raise NotImplementedError(current_cell.reconstruction_type[0] + "is not implemented reconstruction type")

            if current_cell.reconstruction_type[0][0] == "P":  # checks if the first character of the string is "P" implying polynomial reconstruction TODO: Consider moving this into one function
                current_material = current_cell.mat_in_cell[0]
                current_cell.mat_derivative[current_material] = [0]
                if current_cell.reconstruction_type[1] == 0:
                    pass
                elif current_cell.reconstruction_type[1] == 1:
                    current_cell.mat_derivative[current_material][0] = (joined_cells[i+1].get_mat_value(current_material) - joined_cells[i-1].get_mat_value(current_material))/(joined_cells[i+1].get_mat_center(current_material) - joined_cells[i-1].get_mat_center(current_material))
                elif current_cell.reconstruction_type[1] > 1:
                    offset = 0
                    if settings.do_one_sided_reconstruction:
                        if joined_cells[i + 1].reconstruction_type[0] == "mixed":
                            offset = - 1
                        elif joined_cells[i - 1].reconstruction_type[0] == "mixed":
                            offset = 1
                        offset *= current_cell.reconstruction_type[1]//2 - 1

                    current_cell.mat_derivative[current_material] = self.calculate_higher_order_derivatives_in_a_cell(i, joined_cells, current_material, offset)
                else:
                    raise ValueError("Degree of polynomial in reconstruction must be non-negative, degree given is" + current_cell.reconstruction_type[1])

            if current_cell.reconstruction_type[0] == "mixed":
                if current_cell.reconstruction_type[1] == 0:
                    pass
                # elif current_cell.reconstruction_type[1] == 1:
                    #
                    # interface = [x for x in self.list_of_interfaces if (self.nodes[i].position < x < self.nodes[i+1].position)][0]
                    # pass
                else:
                    materials_in_cell = current_cell.get_materials()
                    # print("next is interface")
                    for current_material in materials_in_cell:
                        offset = 0
                        if joined_cells[i + 1].get_materials()[0] == current_material:
                            offset = current_cell.reconstruction_type[1]//2
                        elif joined_cells[i - 1].get_materials()[0] == current_material:
                            offset = - current_cell.reconstruction_type[1]//2
                        # offset *= current_cell.reconstruction_type[1]//2
                        # print("the offset is ",offset)

                        current_cell.mat_derivative[current_material] = self.calculate_higher_order_derivatives_in_a_cell(i, joined_cells, current_material, offset)
                        # print(current_cell.mat_derivative[current_material])



        for i, current_cell in enumerate(self.cells):
            if current_cell.reconstruction_type[0] == "P+BJ":
                current_material = current_cell.mat_in_cell[0]
                if current_material == 0:
                    raise RuntimeError("Called P+BJ in a cell with an interface")
                if current_cell.reconstruction_type[1] > 1:
                    raise NotImplementedError("BJ limiter is only implemented for maximum degree of polynomial of 1 ")
                local_maximum = max(joined_cells[i].get_mat_value(current_material), joined_cells[i+1].get_mat_value(current_material), joined_cells[i-1].get_mat_value(current_material))
                local_minimum = min(joined_cells[i].get_mat_value(current_material), joined_cells[i+1].get_mat_value(current_material), joined_cells[i-1].get_mat_value(current_material))
                phi = 1

                def first_order_tailor(x):
                    return joined_cells[i].get_mat_value(current_material) + joined_cells[i].mat_derivative[current_material][0] * (x - joined_cells[i].mid_point)

                for j in [0, 1]:
                    denominator = first_order_tailor(self.nodes[i+j].position) - joined_cells[i].get_mat_value(current_material)
                    if denominator > 0:
                        phi = min(phi, (local_maximum - joined_cells[i].get_mat_value(current_material))/denominator)
                    elif denominator < 0:
                        phi = min(phi, (local_minimum - joined_cells[i].get_mat_value(current_material))/denominator)

                self.cells[i].mat_derivative[current_material][0] = phi * self.cells[i].mat_derivative[current_material][0]


    def calculate_higher_order_derivatives_in_a_cell(self, index_of_current_cell: int, joined_cells, current_material, offset):
        """
        Outputs the derivatives and changes conservation constants
        """

        degree_of_reconstruction = self.cells[index_of_current_cell].reconstruction_type[1]
        derivatives = np.zeros(degree_of_reconstruction)  # S_i
        conservation_constants = np.zeros(degree_of_reconstruction + 1)  # K_i
        joined_nodes = self.nodes + self.ghost_nodes

        for i in range(2, degree_of_reconstruction + 1):  # calculates the conservation constants, ignoring the first 2 since its analytically 0
            def f(x):  # could be rewritten with lambda function
                return pow((x - self.cells[index_of_current_cell].get_mat_center(current_material)), i)

            left_boundary = self.nodes[index_of_current_cell].position
            right_boundary = self.nodes[index_of_current_cell + 1].position
            current_cell = self.cells[index_of_current_cell]

            interface_list = [k for k in self.list_of_interfaces if left_boundary < k < right_boundary] # should be just 1 element if mixed cell

            if len(interface_list) >= 1:
                if current_cell.get_mat_center(current_material) < interface_list[0]:
                    right_boundary = interface_list[0]
                else:
                    left_boundary = interface_list[0]

            conservation_constants[i] = self.integrate(f, left_boundary, right_boundary) / self.cells[index_of_current_cell].get_mat_volume(current_material)
        self.cells[index_of_current_cell].set_mat_conservation_constants(conservation_constants.tolist(), current_material)

        # calculate how many neighbours are needed
        number_of_neighbours = degree_of_reconstruction
        if number_of_neighbours % 2 != 0:
            number_of_neighbours += 1

        # creating the system
        a = np.zeros((number_of_neighbours, degree_of_reconstruction))
        b = np.zeros(number_of_neighbours)
        nn = - 1
        # print("\n",index_of_current_cell)
        # print(range(index_of_current_cell - number_of_neighbours//2 + offset,  index_of_current_cell + number_of_neighbours//2 + 1 + offset))
        for neighbour_cells_index in range(index_of_current_cell - number_of_neighbours//2 + offset,  index_of_current_cell + number_of_neighbours//2 + 1 + offset):
            # print(neighbour_cells_index, end=" ")

            if neighbour_cells_index == index_of_current_cell:
                pass
            else:
                nn += 1
                for slopes in range(1, degree_of_reconstruction + 1):
                    def f(x):
                        return (x - self.cells[index_of_current_cell].get_mat_center(current_material))**slopes - conservation_constants[slopes]
                    if joined_cells[neighbour_cells_index].mat_in_cell[0] != 0:
                        temp = self.integrate(f, joined_nodes[neighbour_cells_index].position, joined_nodes[neighbour_cells_index + 1].position)
                    else:
                        interface = [k for k in self.list_of_interfaces if joined_nodes[neighbour_cells_index].position < k < joined_nodes[neighbour_cells_index + 1].position][0]
                        if index_of_current_cell < neighbour_cells_index:
                            temp = self.integrate(f, joined_nodes[neighbour_cells_index].position, interface)
                        elif index_of_current_cell > neighbour_cells_index:
                            temp = self.integrate(f, interface, joined_nodes[neighbour_cells_index + 1].position)
                        else:
                            raise RuntimeError("index_of_current_cell == neighbour_cells_index")

                    a[nn, slopes - 1] = temp / self.cells[index_of_current_cell].get_mat_volume(current_material)**(slopes - 1) / joined_cells[neighbour_cells_index].get_mat_volume(current_material)
                b[nn] = joined_cells[neighbour_cells_index].get_mat_value(current_material) - joined_cells[index_of_current_cell].get_mat_value(current_material)

        # calculating pseudoinverse
        psinv = scipy.linalg.pinv(a)

        # calculating result
        result_of_linalg = np.matmul(psinv, b)

        # rescaling
        for i in range(degree_of_reconstruction):
            derivatives[i] = result_of_linalg[i] / self.cells[index_of_current_cell].get_mat_volume(current_material)**i
        # print(derivatives)
        return derivatives.tolist()

    
    def calculate_value(self):
        result = 0
        for i in self.cells:
            result += i.volume * i.average_value
        return result

    def calculate_value_mat(self, material_index):
        if len(self.list_of_materials) < material_index:
            return 0
        result = 0
        for i in self.cells:
            result += i.mat_volume[material_index] * i.mat_average_value[material_index]
        return result



    def l1_error(self):
        original_mesh = function_to_mesh_from_node_list(settings.input_function, self.nodes)
        numerator = 0
        denominator = 0
        for i in range(len(self.cells)):
            numerator += abs(self.cells[i].average_value - original_mesh.cells[i].average_value) * self.cells[i].volume
            denominator += abs(original_mesh.cells[i].average_value) * self.cells[i].volume
        return numerator / denominator

    def l2_error(self):
        original_mesh = function_to_mesh_from_node_list(settings.input_function, self.nodes)
        numerator = 0
        denominator = 0
        for i in range(len(self.cells)):
            numerator += pow(self.cells[i].average_value - original_mesh.cells[i].average_value, 2) * self.cells[i].volume
            denominator += pow(original_mesh.cells[i].average_value, 2) * self.cells[i].volume
        return numerator / denominator

    def l_inf_error(self):  #TODO: je to správně?
        original_mesh = function_to_mesh_from_node_list(settings.input_function, self.nodes)
        numerator = 0
        denominator = 0
        for i in range(len(self.cells)):
            numerator = max(abs(self.cells[i].average_value - original_mesh.cells[i].average_value) * self.cells[i].volume, numerator)
            denominator = max(original_mesh.cells[i].average_value * self.cells[i].volume, denominator)
        return numerator / denominator

    def max_error(self):
        original_mesh = function_to_mesh_from_node_list(settings.input_function, self.nodes)
        result = 0
        for i in range(len(self.cells)):
            result = max(self.cells[i].average_value - original_mesh.cells[i].average_value, result)
        return result

    def l1_error_mat(self, mat):
        original_mesh = function_to_mesh_from_node_list(settings.input_function, self.nodes)
        numerator = 0
        denominator = 0
        for i in range(len(self.cells)):
            if self.cells[i].has_mat(mat):
                numerator += abs(self.cells[i].get_mat_value(mat) - original_mesh.cells[i].get_mat_value(mat)) * self.cells[i].get_mat_volume(mat)
                denominator += abs(original_mesh.cells[i].get_mat_value(mat)) * self.cells[i].get_mat_volume(mat)
        return numerator / denominator

    def l2_error_mat(self, mat):
        original_mesh = function_to_mesh_from_node_list(settings.input_function, self.nodes)
        numerator = 0
        denominator = 0
        for i in range(len(self.cells)):
            if self.cells[i].has_mat(mat):
                numerator += pow(self.cells[i].get_mat_value(mat) - original_mesh.cells[i].get_mat_value(mat), 2) * self.cells[i].get_mat_volume(mat)
                denominator += pow(original_mesh.cells[i].get_mat_value(mat), 2) * self.cells[i].get_mat_volume(mat)
        return numerator / denominator

    def l_inf_error_mat(self, mat):
        original_mesh = function_to_mesh_from_node_list(settings.input_function, self.nodes)
        numerator = 0
        denominator = 0
        for i in range(len(self.cells)):
            if self.cells[i].has_mat(mat):
                numerator = max(abs(self.cells[i].get_mat_value(mat) - original_mesh.cells[i].get_mat_value(mat)) * self.cells[i].get_mat_volume(mat), numerator)
                denominator = max(original_mesh.cells[i].get_mat_value(mat) * self.cells[i].get_mat_volume(mat), denominator)
        return numerator / denominator

    def max_error_mat(self, mat):
        original_mesh = function_to_mesh_from_node_list(settings.input_function, self.nodes)

        result = 0
        for i in range(len(self.cells)):
            if self.cells[i].has_mat(mat):
                result = max(self.cells[i].get_mat_value(mat) - original_mesh.cells[i].get_mat_value(mat), result)
        return result

