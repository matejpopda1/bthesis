import copy
import json

import classes, setting_loader, initialize, draw, lagrange, debug
import matplotlib
import remap
import warnings
import numpy
import os
import multiprocessing
import itertools




# ############################# Problem settings #################################################################
# problem = "const"
# problem = "sod"
# problem = "noh"
# problem = "piston"
# problem = "so"
# problem = "ua_piston"
# problem = "sedov"
# problem = "sedov_new"
# problem = "lax"
# problem = "woodward-collela"


problems = ["sod", "noh", "so", "lax", "woodward-collela", "sedov"]


# strategy = "euler"
# strategy = "lagrange"
# strategy = "ale"

strategies = ["ale"]

# mesh_dimensions = "50"
# mesh_dimensions = "100"
# mesh_dimensions = "300"

dimensions = ["50", "100", "200" ]

# mesh_type = "equidistant"

types = ["equidistant"]

# remap_settings = "P4"
# remap_settings = "const"
# remap_settings = "P1"
# remap_settings = "P1lim"
# remap_settings = "P4THINC"
# remap_settings = "P4JUMP"
# remap_settings = "P4lim"


remaps = ["P4", "P4THINC", "P4JUMP", "P1lim"]



# ################################# Settings relating to warnings and pycharm ######################################
# Changes drawing to be in separate window
# matplotlib.use('Qt5Agg')


# Should raise exception in case of a NaN (Only for Numpy)
numpy.seterr(invalid='raise')


# warnings.simplefilter('always')
warnings.filterwarnings("ignore", message='Extremely bad integrand*')
####################################################################################




def job(problem, strategy, mesh_dimensions, mesh_type, remap_settings):
    draw_after_time = 5

    draw_initialization = False
    draw_every_step_after_target_time = False
    draw_waves_at_the_end = True
    draw_reconstruction = True

    save_results = True
    draw_results = False

    print("Currently solving the " + problem + " problem with " + strategy + " rezone on a " + mesh_dimensions + " cell " + mesh_type + " mesh")
    settings = setting_loader.load_settings(problem, strategy, mesh_dimensions, mesh_type, remap_settings)


    identifier = problem + "-" + remap_settings + "-" + strategy + "-" + mesh_dimensions + "-" + mesh_type
    folder = "Results/"+identifier
    if save_results:
        path = os.path.join(os.path.dirname(__file__), folder)
        if not os.path.exists(path):
            os.makedirs(path)

    mesh, runtime = initialize.initialize(settings)
    history = classes.DrawHistory()

    initial_mesh = copy.deepcopy(mesh)


    if not draw_initialization:
        draw.drawing_target_time += draw_after_time

    while runtime.elapsed_time < mesh.params.final_time:
        try:
            mesh = lagrange.step(mesh, runtime, history)

        except classes.NegativeEnergyError:
            runtime.dt = runtime.dt * runtime.c_dt_decrease

        if runtime.current_timestep % mesh.params.remap_after_steps == 0:
            mesh = remap.rezone_and_remap(mesh)

    print("!Done with: " + identifier)
    fig, points_for_drawing = draw.draw_all_quantities(mesh, runtime, draw_ghost_cells=False, draw_waves=True,
                                   time_increment=draw_after_time, draw_shock=True,
                                   draw_reconstruction=draw_reconstruction)
    if save_results:
        fig.savefig(folder + "/" + "final" + ".svg", format='svg', dpi=1200)
    fig.savefig("Results/final/" + identifier + ".svg", format='svg', dpi=1200)
    json.dump(points_for_drawing, open("raws/" + identifier + ".json", 'w'))
    if draw_waves_at_the_end:
        fig = draw.wave_history(history)
        fig.savefig(folder + "/" + "waves" + ".svg", format='svg', dpi=1200)


if __name__ == "__main__":
    print("started")
    # create processes

    processes = []

    for args in itertools.product(problems, strategies, dimensions, types, remaps):
        print(args)
        a = multiprocessing.Process(target=job, args=args)
        a.start()
    # multiprocessing.Process(target=job, args=('sod', 'euler', '50', 'equidistant', 'P4')).start()
