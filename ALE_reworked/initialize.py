import math

import classes
from mesh import Mesh, Wave
import scipy.integrate as integrate


def initialize(params: classes.Initialization):
    mesh = Mesh(params)

    initialize_mesh_geometry(mesh)
    initialize_problem(mesh)

    runtime = classes.Runtime()
    runtime.c_dt_increase = params.initial_dt_increase

    return mesh, runtime


def initialize_mesh_geometry(mesh: Mesh):
    if mesh.params.initial_mesh_type == "equidistant":
        initialize_mesh_equidistant(mesh)
    else:
        raise ValueError("Initialization doesn't contain known initial mesh type")


def initialize_mesh_equidistant(mesh: Mesh):
    params = mesh.params
    dx = (params.right_boundary - params.left_boundary) / params.number_of_cells
    for node in mesh.nodes:
        node.x = params.left_boundary + node.self_index * dx


def initialize_problem(mesh: Mesh):
    if mesh.params.problem_type == "sod":
        initialize_sod(mesh)
    elif mesh.params.problem_type == "piston":
        initialize_piston(mesh)
    elif mesh.params.problem_type == "noh":
        initialize_noh(mesh)
    elif mesh.params.problem_type == "sedov":
        initialize_sedov(mesh)
    elif mesh.params.problem_type == "sedov_centered":
        initialize_sedov_centered(mesh)
    elif mesh.params.problem_type == "lax":
        initialize_lax(mesh)
    elif mesh.params.problem_type == "so":
        initialize_so(mesh)
    elif mesh.params.problem_type == "wc":
        initialize_wc(mesh)
    elif mesh.params.problem_type == "const":
        initialize_const(mesh)
    elif mesh.params.problem_type == "ua_piston":
        initialize_ua_piston(mesh)
    else:
        raise ValueError("Initialization doesn't contain known problem")


def initialize_const(mesh: Mesh):
    for cell in mesh.cells:
        cell.velocity = 0
        cell.mass = 1 * cell.volume
        cell.density = 1
        cell.pressure = 1


def initialize_sod(mesh: Mesh):

    def step_density(x):
        if x < 0.5:
            return 1
        return 0.125

    def step_pressure(x):
        if x < 0.5:
            return 1
        return 0.1

    for cell in mesh.cells:
        cell.velocity = 0
        if cell.edge_nodes()[1].x < 0.5 or cell.edge_nodes()[0].x > 0.5:
            cell.mass = step_density(cell.x) * cell.volume
            cell.density = step_density(cell.x)
            cell.pressure = step_pressure(cell.x)
        else:
            l_node, r_node = cell.edge_nodes()
            cell.mass = integrate.quad(step_density, l_node.x, r_node.x)[0]
            cell.density = integrate.quad(step_density, l_node.x, r_node.x)[0] / cell.volume
            cell.pressure = integrate.quad(step_pressure, l_node.x, r_node.x)[0] / cell.volume

    shock = Wave(mesh)
    shock.position = 0.5
    mesh.waves.append(shock)


def initialize_lax(mesh: Mesh):

    def step_pressure(x):
        if x < 0.5:
            return 1000
        return 0.01

    for cell in mesh.cells:
        cell.velocity = 0
        cell.density = 1
        cell.mass = cell.density * cell.volume
        if cell.edge_nodes()[1].x < 0.5 or cell.edge_nodes()[0].x > 0.5:
            cell.pressure = step_pressure(cell.x)
        else:
            l_node, r_node = cell.edge_nodes()
            cell.pressure = integrate.quad(step_pressure, l_node.x, r_node.x)[0] / cell.volume

    shock = Wave(mesh)
    shock.position = 0.5
    mesh.waves.append(shock)


def initialize_wc(mesh: Mesh):
    def step_pressure(x):
        if x < 0.1:
            return 1000
        elif x > 0.9:
            return 100
        return 0.01

    for cell in mesh.cells:
        cell.velocity = 0
        cell.density = 1
        cell.mass = 1 * cell.volume
        if not ((cell.edge_nodes()[1].x > 0.1 > cell.edge_nodes()[0].x) or (cell.edge_nodes()[1].x > 0.9 > cell.edge_nodes()[0].x)):
            cell.pressure = step_pressure(cell.x)
        else:
            l_node, r_node = cell.edge_nodes()
            cell.pressure = integrate.quad(step_pressure, l_node.x, r_node.x)[0] / cell.volume

    shock = Wave(mesh)
    shock.position = 0.9
    mesh.waves.append(shock)

    shock = Wave(mesh)
    shock.position = 0.1
    mesh.waves.append(shock)


def initialize_noh(mesh: Mesh):
    initial_shock_position = 0

    shock = Wave(mesh)
    shock.position = initial_shock_position
    mesh.waves.append(shock)

    def step_velocity(x):
        if x < initial_shock_position:
            return 1
        return -1

    for cell in mesh.cells:
        cell.mass = 1 * cell.volume
        cell.density = 1
        cell.pressure = 10**-6

        if cell.edge_nodes()[1].x < 0.5 or cell.edge_nodes()[0].x > 0.5:
            cell.velocity = step_velocity(cell.x)
        else:
            l_node, r_node = cell.edge_nodes()
            cell.velocity = integrate.quad(step_velocity, l_node.x, r_node.x)[0] / cell.volume


def initialize_sedov(mesh: Mesh):
    for cell in mesh.cells:
        cell.velocity = 0
        cell.density = 1
        cell.mass = cell.density * cell.volume
        cell.pressure = 10**-6

    left_cell = mesh.first_regular_cell

    left_cell.energy = 0.244816 / left_cell.volume
    left_cell.pressure = left_cell.energy * left_cell.density * (mesh.params.gamma - 1)

    shock = Wave(mesh)
    shock.position = mesh.get_node(1).x
    mesh.waves.append(shock)


def initialize_sedov_centered(mesh: Mesh):
    for cell in mesh.cells:
        cell.velocity = 0
        cell.density = 1
        cell.mass = cell.density * cell.volume
        cell.pressure = 10**-6

    left_cell = mesh.get_cell(mesh.params.number_of_cells // 2)
    right_cell = mesh.get_cell(mesh.params.number_of_cells // 2 + 1)

    left_cell.energy = (0.244816 / 2) / left_cell.volume
    left_cell.pressure = left_cell.energy * left_cell.density * (mesh.params.gamma - 1)
    right_cell.energy = (0.244816 / 2) / right_cell.volume
    right_cell.pressure = right_cell.energy * right_cell.density * (mesh.params.gamma - 1)

    shock = Wave(mesh)
    shock.position = left_cell.edge_nodes()[0].x
    mesh.waves.append(shock)

    shock = Wave(mesh)
    shock.position = right_cell.edge_nodes()[1].x
    mesh.waves.append(shock)


"""
For uniformly accelerated piston: https://phys.au.dk/~srf/hydro/Landau+Lifschitz.pdf page 384 (396 in pdf)
TODO: crashes for small cell pressure
"""


def initialize_ua_piston(mesh: Mesh):
    for cell in mesh.cells:
        cell.velocity = 0
        cell.mass = 1 * cell.volume
        cell.density = 1
        cell.pressure = 1


def initialize_piston(mesh: Mesh):
    energy_in_cells = 10**-6
    for cell in mesh.cells:
        cell.velocity = 0
        cell.mass = 1 * cell.volume
        cell.density = 1
        cell.pressure = energy_in_cells * cell.density * (mesh.params.gamma - 1)


def initialize_so(mesh: Mesh):
    initial_shock_position = -4

    shock = Wave(mesh)
    shock.position = initial_shock_position
    mesh.waves.append(shock)

    def density(x):
        if x < initial_shock_position:
            return 3.857143
        return 1 + 0.2 * math.sin(5*x)

    def velocity(x):
        if x < initial_shock_position:
            return 2.629369
        return 0

    def pressure(x):
        if x < initial_shock_position:
            return 10.33333
        return 1

    for cell in mesh.cells:
        if cell.edge_nodes()[1].x < initial_shock_position:
            cell.mass = density(cell.x) * cell.volume
            cell.density = density(cell.x)
        elif cell.edge_nodes()[0].x > initial_shock_position:
            l_node, r_node = cell.edge_nodes()
            cell.mass = integrate.quad(density, l_node.x, r_node.x)[0]
            cell.density = cell.mass / cell.volume

        if cell.edge_nodes()[1].x < initial_shock_position or cell.edge_nodes()[0].x > initial_shock_position:
            cell.velocity = velocity(cell.x)
            cell.pressure = pressure(cell.x)
        else:
            l_node, r_node = cell.edge_nodes()
            cell.velocity = integrate.quad(velocity, l_node.x, r_node.x)[0] / cell.volume
            cell.mass = integrate.quad(density, l_node.x, r_node.x)[0]
            cell.density = integrate.quad(density, l_node.x, r_node.x)[0] / cell.volume
            cell.pressure = integrate.quad(pressure, l_node.x, r_node.x)[0] / cell.volume



