class Constants:
    EPS_dvdt = 10**-16
    EPS_BJ_LIMITER = 10**-16
    EPS_min_dt = 10**-8
    minimum_energy = 10**-10
    CFL = 0.25
    CFL_volume = 0.1


class Runtime:  # Class that contains parameters that change during run, defaults can be set-up here
    def __init__(self):
        self.c_dt_increase = None # 1.01
        self.c_dt_decrease = 0.5
        self.current_timestep = 0
        self.elapsed_time = 0
        self.dt = None


class Initialization:  # Class that contains parameters that change only during initialization
    def __init__(self):
        self.number_of_cells = None
        self.number_of_ghost_cells = None  # just to one side
        self.left_boundary = None
        self.right_boundary = None
        self.initial_mesh_type = None
        self.problem_type = None
        self.final_time = None
        self.boundary_condition = None
        self.boundary_parameters = {
            "left_piston_speed": None
        }
        self.gamma = None
        self.remap_after_steps = None
        self.rezone_type = None
        self.default_reconstruction = None
        self.reconstruction_around_shock = None
        self.reconstruction_in_wave = None
        self.reconstruction_around_wave = None
        self.THINC_beta = None
        self.initial_dt_increase = None


class DrawHistory:
    def __init__(self):
        self.time = []
        self.waves_pos = []
        self.shock_pos = []


class NegativeEnergyError(Exception):
    pass
