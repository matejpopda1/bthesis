from mesh import Mesh
from mesh import Cell
from mesh import Wave
import classes
from mesh import copy_mesh

import debug
# TEMP
import initialize


# TODO: ghost cells should be recreated every step

def step(mesh: Mesh, runtime: classes.Runtime, history: classes.DrawHistory):
    update_ghost_cells(mesh)
    for i in range(2):
        calculate_subcell_impedances(mesh, i)
        reconstruct_velocity_and_pressure(mesh)
        riemann_solver(mesh)

        boundary_conditions(mesh, runtime)

        calculate_dt(mesh, runtime)

        update(mesh, runtime)

        boundary_conditions(mesh, runtime)  # Might be unnecessary

    new_mesh = copy_mesh(mesh)

    # debug.left_two_cells_info(mesh)

    solve_euler(mesh, new_mesh, runtime)

    update_ghost_cells(new_mesh)

    update_waves(mesh, new_mesh, runtime)

    detect_compression(mesh, new_mesh, runtime)

    update_history(new_mesh, runtime, history)

    runtime.current_timestep += 1
    runtime.elapsed_time += runtime.dt

    return new_mesh


def update_ghost_cells(mesh: Mesh):
    left_cell = mesh.get_cell(0)
    right_cell = mesh.get_cell(mesh.params.number_of_cells - 1)
    for cell in mesh.ghost_cells():
        if cell.x < (left_cell.x + right_cell.x) / 2:
            cell.density = left_cell.density
            cell.velocity = left_cell.velocity
            cell.pressure = left_cell.pressure
            cell.energy = left_cell.energy
        else:
            cell.density = right_cell.density
            cell.velocity = right_cell.velocity
            cell.pressure = right_cell.pressure
            cell.energy = right_cell.energy
    move_ghost_cells(mesh)


def move_ghost_cells(mesh: Mesh):
    left_nodes = [x for x in mesh.ghost_nodes() if x.x < mesh.first_regular_node.x]
    right_nodes = [x for x in mesh.ghost_nodes() if x.x > mesh.last_regular_node.x]

    dx = mesh.first_regular_cell.volume
    for i, node in enumerate(left_nodes[::-1]):
        node.x = mesh.first_regular_node.x - dx * (i+1)

    dx = mesh.last_regular_cell.volume
    for i, node in enumerate(right_nodes):
        node.x = mesh.last_regular_node.x + dx * (i+1)




def calculate_subcell_impedances(mesh: Mesh, iteration):
    if iteration == 0:
        for cell in mesh.cells:
            cell.left_impedance = cell.acoustic_impedance
            cell.right_impedance = cell.acoustic_impedance
    else:
        gamma = (mesh.params.gamma + 1)/2
        for cell in mesh.regular_cells():  # TODO wrong?
            lnode, rnode = cell.edge_nodes()
            cell.left_impedance = cell.acoustic_impedance + gamma * abs(cell.velocity - lnode.predicted_velocity)
            cell.right_impedance = cell.acoustic_impedance + gamma * abs(cell.velocity - rnode.predicted_velocity)


def reconstruct_velocity_and_pressure(mesh: Mesh):
    for cell in mesh.cells[1:-1]:
        cell.pressure_derivative = reconstruction(cell, "pressure")
        cell.velocity_derivative = reconstruction(cell, "velocity")


# TODO: rewrite to be nicer, ie better names for arr, and other variables
def reconstruction(cell, variable):
    left_neighbor, right_neighbor = cell.exclusive_cell_neighbors(1)

    lnode, rnode = cell.edge_nodes()

    lnv = getattr(left_neighbor, variable)
    rnv = getattr(right_neighbor, variable)
    snv = getattr(cell, variable)

    nu = rnv - lnv
    de = right_neighbor.x - left_neighbor.x
    d = nu/de

    amin = min(lnv, rnv, snv)
    amax = max(lnv, rnv, snv)

    unal = snv + d * (lnode.x - cell.x)
    unar = snv + d * (rnode.x - cell.x)

    phil = venkatakrishnan(unal, amin, amax, snv)
    phir = venkatakrishnan(unar, amin, amax, snv)
    phi = min(phir, phil)
    return d * phi


def venkatakrishnan(awhat, amin, amax, ubar):
    if awhat > ubar:
        y = (amax-ubar)/(awhat-ubar + classes.Constants.EPS_BJ_LIMITER)
        return (y**2 + 2*y) / (y**2 + y + 2)
    elif awhat < ubar:
        y = (amin - ubar) / (awhat-ubar + classes.Constants.EPS_BJ_LIMITER)
        return (y**2 + 2*y) / (y**2 + y + 2)
    else:
        return 1


def riemann_solver(mesh: Mesh):
    for node in mesh.regular_nodes():
        left_cell = node.left_cell
        right_cell = node.right_cell
        pl = left_cell.pressure + left_cell.pressure_derivative * (node.x - left_cell.x)
        pr = right_cell.pressure + right_cell.pressure_derivative * (node.x - right_cell.x)

        ul = left_cell.velocity + left_cell.velocity_derivative * (node.x - left_cell.x)
        ur = right_cell.velocity + right_cell.velocity_derivative * (node.x - right_cell.x)

        zl = left_cell.right_impedance
        zr = right_cell.left_impedance

        node.riemann_velocity = (zl * ul + zr * ur) / (zl + zr) - (pr - pl) / (zl + zr)
        node.riemann_pressure = (zl * pr + zr * pl) / (zl + zr) - (zl * zr) / (zl + zr) * (ur - ul)


def calculate_dt(mesh: Mesh, runtime: classes.Runtime):
    if runtime.elapsed_time == 0:
        runtime.dt = mesh.params.final_time * 10**-6
        return

    dt_prev = runtime.dt

    # CFL
    dt_lag = 10**20
    for cell in mesh.regular_cells():
        dtc = classes.Constants.CFL * cell.volume / cell.speed_of_sound
        dt_lag = min(dtc, dt_lag)

    # PHM
    dt_vol = 10**20
    for cell in mesh.regular_cells():
        dvdt = cell.edge_nodes()[1].riemann_velocity - cell.edge_nodes()[0].riemann_velocity
        dtc = classes.Constants.CFL_volume * cell.volume / (abs(dvdt) + classes.Constants.EPS_dvdt)
        dt_vol = min(dtc, dt_vol)

    runtime.dt = min(dt_lag, dt_vol, runtime.c_dt_increase*dt_prev)

    if runtime.dt < 0:
        raise RuntimeError("Negative timestep")

    if runtime.elapsed_time + runtime.dt > mesh.params.final_time:
        runtime.dt = mesh.params.final_time - runtime.elapsed_time


def update(mesh: Mesh, runtime: classes.Runtime):
    for node in mesh.regular_nodes():
        left_cell = node.left_cell
        right_cell = node.right_cell

        zl = left_cell.right_impedance
        zr = right_cell.left_impedance

        cl = left_cell.speed_of_sound
        cr = right_cell.speed_of_sound

        dpl = left_cell.pressure_derivative
        dpr = right_cell.pressure_derivative
        dul = left_cell.velocity_derivative
        dur = right_cell.velocity_derivative

        dun_rs = - (cl * (dpl + zl * dul) + cr * (dpr - zr * dur)) / (zl + zr)
        dpn_rs = (cr * (dpr - zr * dur) * zl - cl * (dpl + zl * dul) * zr) / (zl + zr)

        node.predicted_velocity = node.riemann_velocity + runtime.dt/2 * dun_rs
        node.predicted_pressure = node.riemann_pressure + runtime.dt/2 * dpn_rs


def solve_euler(mesh: Mesh, new_mesh: Mesh, runtime: classes.Runtime):
    for old_cell, new_cell in zip(mesh.regular_cells(), new_mesh.regular_cells()):
        if old_cell.self_index != new_cell.self_index:
            raise RuntimeError("Indexes of cells don't match")
        l_node, r_node = old_cell.edge_nodes()
        new_cell.density = 1 / (1/old_cell.density + runtime.dt/old_cell.mass * (r_node.predicted_velocity - l_node.predicted_velocity))
        new_cell.velocity = old_cell.velocity - runtime.dt/old_cell.mass * (r_node.predicted_pressure - l_node.predicted_pressure)

        old_specific_total_energy = old_cell.energy + 1/2 * old_cell.velocity**2
        new_specific_total_energy = old_specific_total_energy - runtime.dt/old_cell.mass * (r_node.predicted_pressure*r_node.predicted_velocity - l_node.predicted_pressure*l_node.predicted_velocity)
        new_cell.energy = new_specific_total_energy - 1/2 * new_cell.velocity**2

        if new_cell.energy < 0:
            raise classes.NegativeEnergyError

    for old_ghost, new_ghost in zip(mesh.ghost_nodes(), new_mesh.ghost_nodes()):
        new_ghost.x = old_ghost.x


    for old_node, new_node in zip(mesh.regular_nodes(), new_mesh.regular_nodes()):
        new_node.x = old_node.x + runtime.dt*old_node.predicted_velocity

    for old_cell, new_cell in zip(mesh.cells, new_mesh.cells):
        new_cell.mass = old_cell.mass

    for new_cell in new_mesh.regular_cells():
        cell_update_pressure(new_cell)


def update_waves(mesh: Mesh, new_mesh: Mesh, runtime: classes.Runtime):
    def move_wave(wave: Wave):
        result = Wave(new_mesh)

        left_node, right_node = wave.get_cell_the_wave_is_in().edge_nodes()

        relative_wave_pos = (wave.position - left_node.x)/(right_node.x - left_node.x)
        velocity = left_node.predicted_velocity * (1 - relative_wave_pos) + right_node.predicted_velocity * relative_wave_pos

        dx = velocity * runtime.dt
        result.position = wave.position + dx
        return result

    for old, new in zip(mesh.waves, new_mesh.waves):
        new.position = move_wave(old).position


def cell_update_pressure(cell: Cell):
    def eos_pres(e, rho, gamma):
        return e * rho * (gamma - 1)
    cell.pressure = eos_pres(cell.energy, cell.density, cell.parent_mesh.params.gamma)


def boundary_conditions(mesh: Mesh, runtime: classes.Runtime):
    if mesh.params.boundary_condition == "free":
        return
    elif mesh.params.boundary_condition == "walls":
        bc_wall(mesh, left=True, right=True)
    elif mesh.params.boundary_condition == "left_constant_piston":
        bc_piston_const(mesh, runtime)
    elif mesh.params.boundary_condition == "left_accelerated_piston":
        bc_piston_acc(mesh, runtime)
    else:
        raise RuntimeError("Unknown boundary condition")


def bc_piston_const(mesh: Mesh, runtime: classes.Runtime):
    bc_wall(mesh, left=False, right=True)
    mesh.first_regular_node.predicted_velocity = mesh.params.boundary_parameters["left_piston_speed"]
    mesh.first_regular_node.riemann_velocity = mesh.params.boundary_parameters["left_piston_speed"]


# From Maire
def bc_piston_acc(mesh: Mesh, runtime: classes.Runtime):
    bc_wall(mesh, left=False, right=True)
    mesh.first_regular_node.predicted_velocity = runtime.elapsed_time * 1/2
    mesh.first_regular_node.riemann_velocity = runtime.elapsed_time * 1/2


def bc_wall(mesh: Mesh, left, right):
    if left:
        mesh.first_regular_node.predicted_velocity = 0
        mesh.first_regular_node.riemann_velocity = 0
    if right:
        mesh.last_regular_node.predicted_velocity = 0
        mesh.last_regular_node.riemann_velocity = 0



def detect_compression(old_mesh, new_mesh, runtime):
    dt = runtime.dt
    parameter = 0.95

    for i in range(old_mesh.params.number_of_cells):
        cell = old_mesh.get_cell(i)
        left, right = cell.edge_nodes()
        # print((left.predicted_velocity - right.predicted_velocity), ((parameter - 1) * cell.volume / dt)) #TODO HERE
        if (right.predicted_velocity - left.predicted_velocity) < ((parameter - 1) * cell.volume / dt):
            new_mesh.get_cell(i).compressed = True


def update_history(mesh: Mesh, runtime: classes.Runtime, history: classes.DrawHistory):
    history.time.append(runtime.elapsed_time)
    history.waves_pos.append([x.position for x in mesh.waves])

    history.shock_pos.append(list(x.x for x in mesh.cells_with_compression(runtime.dt)))
