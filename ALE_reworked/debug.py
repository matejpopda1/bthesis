from mesh import Mesh
from classes import Runtime
from mesh import Cell


def write_out(mesh: Mesh, runtime: Runtime):
    result = "timestep: {timestep},\t elapsed time: {etime},\t current dt: {dt},\t total mass: {mass},\t total momentum: {momentum},\t total energy: {energy}, \t waves: {waves}"
    result = result.format(timestep=runtime.current_timestep,
                           dt=runtime.dt,
                           etime=runtime.elapsed_time,
                           mass=mesh.total_mass(),
                           momentum=mesh.total_momentum(),
                           energy=mesh.total_energy(),
                           waves=[wave.position for wave in mesh.waves])
    print(result)


def cell_info(mesh: Mesh, index: int):
    cell = mesh.get_cell(index)
    result = "Values of cell {index} are: \n" \
             "Density: {density}, velocity: {velocity}, pressure: {pressure}, energy:{energy}\n" \
             "The predicted and riemann velocity in left node are: {lpred} and {lriem}\n" \
             "In right: {rpred} and {rriem}"

    result = result.format(index=index,
                           density=cell.density,
                           velocity=cell.velocity,
                           pressure=cell.pressure,
                           lpred=cell.edge_nodes()[0].predicted_velocity,
                           rpred=cell.edge_nodes()[1].predicted_velocity,
                           lriem=cell.edge_nodes()[0].riemann_velocity,
                           rriem=cell.edge_nodes()[1].riemann_velocity,
                           energy=cell.energy
                           )
    print(result)


def left_two_cells_info(mesh):
    cell_info(mesh, -1)
    print("\n")
    cell_info(mesh, 0)
    print("\n")
    cell_info(mesh, 1)
    print("\n\n\n\n\n")