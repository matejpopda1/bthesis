import math
import typing  # Should work everywhere but might be version dependent
import classes
import functools
import copy


class Node:
    def __init__(self, parent_mesh, self_index):
        self.parent_mesh: Mesh = parent_mesh
        self.self_index = self_index
        self.x = None
        # self.mass = None
        # self.velocity = None
        self.predicted_velocity = None
        self.predicted_pressure = None
        self.riemann_velocity = None
        self.riemann_pressure = None

    def __str__(self):
        return str(self.x)

    @property
    def left_cell(self) -> 'Cell':
        return self.parent_mesh.get_cell(self.self_index - 1)

    @property
    def right_cell(self) -> 'Cell':
        return self.parent_mesh.get_cell(self.self_index)


class Cell:
    def __init__(self, parent_mesh, self_index):

        self.parent_mesh: Mesh = parent_mesh
        self.self_index = self_index
        self.mass = None
        # self.lc_mass = None
        # self.rc_mass = None
        self.density = None
        self.velocity = None
        self.pressure = None
        # self.viscosity = None
        # self.l_force = None
        # self.r_force = None
        self.left_impedance = None
        self.right_impedance = None
        self.pressure_derivative = None
        self.velocity_derivative = None
        self._energy = None

        self.kinetic_energy = None
        self.momentum = None
        self.momentum_density = None
        self.internal_energy_density = None
        self.total_internal_energy = None
        self.kinetic_energy_density = None

        self.compressed: bool = False

        # parameters for remap
        # Polynomial

        self.derivative = {
            # "density": [],
            # "velocity": [],
            # "pressure": [],
            # "energy": []
        }
        self.reconstruction_type = {}

        self.conservation_constants = []

        # THINC

        self.theta = {}
        self.hyperbolic_conservation_constant = {}
        self.beta = {}
        self.a_min = {}
        self.a_max = {}

        # JUMP
        self.jump_position = {}
        self.left_val = {}
        self.right_val = {}

    @property
    def x(self):
        l_node, r_node = self.edge_nodes()
        return (l_node.x + r_node.x)/2

    @property
    def volume(self):
        l_node, r_node = self.edge_nodes()
        return r_node.x - l_node.x

    @property
    def energy(self):
        if self._energy is None:
            self._energy = max(self.pressure / ((self.parent_mesh.params.gamma - 1) * self.density), classes.Constants.minimum_energy)
        return self._energy

    @energy.setter
    def energy(self, value):
        self._energy = value

    @property
    def speed_of_sound(self):
        return math.sqrt((self.parent_mesh.params.gamma - 1) * self.parent_mesh.params.gamma * self.energy)

    @property
    def acoustic_impedance(self):
        return self.speed_of_sound * self.density

    @property
    def neighbors(self):
        return [self.parent_mesh.get_cell(self.self_index - 1), self.parent_mesh.get_cell(self.self_index + 1)]

    @property
    def left_neighbor(self):
        return self.parent_mesh.get_cell(self.self_index - 1)

    @property
    def right_neighbor(self):
        return self.parent_mesh.get_cell(self.self_index + 1)


    def exclusive_cell_neighbors(self, size):
        """
        size: how many neighbors to grab to each side
        """
        if size == 1:
            return self.neighbors

        return [i for i in self.parent_mesh.cells if (i.self_index != self.self_index and abs(i.self_index - self.self_index) <= size)]

    def edge_nodes(self):
        return self.parent_mesh.get_node(self.self_index), self.parent_mesh.get_node(self.self_index + 1)

    @property
    def left_node(self):
        return self.parent_mesh.get_node(self.self_index)

    @property
    def right_node(self):
        return self.parent_mesh.get_node(self.self_index + 1)

    def is_x_in(self, x):
        nodes = self.edge_nodes()
        return nodes[0].x < x <= nodes[1].x

    def reset_energy(self):
        self._energy = None


    def get_quantity_remap(self, quantity):
        if quantity == "density":
            return self.density
        elif quantity == "momentum_density":
            return self.momentum_density
        elif quantity == "internal_energy_density":
            return self.internal_energy_density
        elif quantity == "kinetic_energy_density":
            return self.kinetic_energy_density
        else:
            raise RuntimeError("Invalid quantity")

    def set_quantity_remap(self, quantity, x):
        if quantity == "density":
            self.mass = x
        elif quantity == "momentum_density":
            self.momentum = x
        elif quantity == "internal_energy_density":
            self.total_internal_energy = x
        elif quantity == "kinetic_energy_density":
            self.kinetic_energy = x
        else:
            raise RuntimeError("Invalid quantity")



class Nodes(typing.List[Node]):
    def __init__(self, parent):
        super(Nodes, self).__init__()
        self.parent_mesh: Mesh = parent


class Cells(typing.List[Cell]):
    def __init__(self, parent_mesh):
        super(Cells, self).__init__()
        self.parent_mesh: Mesh = parent_mesh


class Wave:
    def __init__(self, parent_mesh):
        self.position = None
        self.parent_mesh: Mesh = parent_mesh

    def get_cell_the_wave_is_in(self):
        return [cell for cell in self.parent_mesh.cells if cell.is_x_in(self.position)][0]  # TODO: make more robust, plus test if there is only one cell etc etc


class Waves(typing.List[Wave]):
    def __init__(self, parent):
        super(Waves, self).__init__()
        self.parent_mesh: Mesh = parent


class Mesh:
    def __init__(self, params):
        self.nodes = Nodes(self)
        self.cells = Cells(self)
        self.waves = Waves(self)
        self.params:  classes.Initialization = params

        for i in range(params.number_of_cells + 2 * params.number_of_ghost_cells):
            self.cells.append(Cell(self, i - params.number_of_ghost_cells))
        for i in range(params.number_of_cells + 1 + 2 * params.number_of_ghost_cells):
            self.nodes.append(Node(self, i - params.number_of_ghost_cells))

    # TODO: following can be cached but maybe it can be made during the initialization
    def regular_cells(self):  # returns non-ghost cells
        return [i for i in self.cells if -1 < i.self_index < self.params.number_of_cells]
        # return self.cells[self.params.number_of_ghost_cells: -self.params.number_of_ghost_cells] # TODO: this might be faster but no reason to change for now

    def ghost_cells(self):  # returns ghost cells
        return [i for i in self.cells if not (-1 < i.self_index < self.params.number_of_cells)]

    def regular_nodes(self):
        return [i for i in self.nodes if -1 < i.self_index < self.params.number_of_cells + 1]

    def ghost_nodes(self):
        return [i for i in self.nodes if not (-1 < i.self_index < self.params.number_of_cells + 1)]

    def get_cell(self, i):
        if i < -self.params.number_of_ghost_cells or i > self.params.number_of_cells + self.params.number_of_ghost_cells:
            raise IndexError("Trying to access a cell out of bounds")
        return self.cells[i + self.params.number_of_ghost_cells]

    def get_node(self, i):
        if i < -self.params.number_of_ghost_cells or i > self.params.number_of_cells + self.params.number_of_ghost_cells + 1:
            raise IndexError("Trying to access a cell out of bounds")
        return self.nodes[i + self.params.number_of_ghost_cells]

    def total_mass(self):
        return sum(cell.mass for cell in self.regular_cells())

    def total_momentum(self):
        return sum(cell.velocity * cell.mass for cell in self.regular_cells())

    def total_energy(self):
        return sum(cell.energy*cell.mass + 1/2*cell.mass*cell.velocity**2 for cell in self.regular_cells())

    @property
    def first_regular_cell(self):
        return self.get_cell(0)

    @property
    def last_regular_cell(self):
        return self.get_cell(self.params.number_of_cells - 1)

    @property
    def first_regular_node(self):
        return self.get_node(0)

    @property
    def last_regular_node(self):
        return self.get_node(self.params.number_of_cells)

    def cells_with_compression(self, dt):
        result = []
        if self.first_regular_node.predicted_velocity is None:  # edge case for first timestep
            return result

        for i in self.regular_cells():
            if i.compressed:
                result.append(i)
        return result


def copy_mesh(input_mesh: Mesh):
    result = Mesh(input_mesh.params)
    for new_node, old_node in zip(result.nodes, input_mesh.nodes):
        new_node.x = old_node.x
        new_node.predicted_velocity = old_node.predicted_velocity

    for wave in input_mesh.waves:
        new_wave = Wave(result)
        new_wave.position = wave.position
        result.waves.append(new_wave)

    return result


if __name__ == "__main__":
    import setting_loader

