import matplotlib.pyplot as plt
from mesh import Mesh
from classes import Runtime
from classes import DrawHistory

from matplotlib import collections as mc
import remap


drawing_target_time = 0


def draw_all_quantities(mesh: Mesh, runtime: Runtime, draw_ghost_cells=False, draw_waves=False, time_increment=0, draw_shock=False, draw_reconstruction=False):
    global drawing_target_time
    drawing_target_time += time_increment

    fig, axs = plt.subplots(2, 2)



    fig.suptitle("Elapsed time = " + str(runtime.elapsed_time))
    fig.tight_layout(h_pad=2)

    if not draw_ghost_cells:
        cells = mesh.regular_cells()
    else:
        cells = mesh.cells


    points_for_drawing = {"points": [cell.x for cell in cells]}
    axs[0, 0].set_title("Density")
    axs[0, 0].plot([cell.x for cell in cells], [cell.density for cell in cells], color="black")
    points_for_drawing["density"] = [cell.density for cell in cells]


    axs[0, 1].set_title("Internal energy")
    axs[0, 1].plot([cell.x for cell in cells], [cell.energy for cell in cells], color="r")
    points_for_drawing["internal_energy"] = [cell.density for cell in cells]


    axs[1, 0].set_title("Pressure")
    axs[1, 0].plot([cell.x for cell in cells], [cell.pressure for cell in cells], color="b")
    points_for_drawing["pressure"] = [cell.density for cell in cells]


    axs[1, 1].set_title("Velocity")
    axs[1, 1].plot([cell.x for cell in cells], [cell.velocity for cell in cells], color="g")
    points_for_drawing["velocity"] = [cell.density for cell in cells]


    if draw_waves:
        for row in axs:
            for ax in row:
                for i in mesh.waves:
                    ax.axvline(i.position)

    if draw_shock:
        for cell in mesh.cells_with_compression(runtime.dt):
            for row in axs:
                for ax in row:
                    boundary = []
                    for i in cell.edge_nodes():
                        boundary.append(i.x)
                    ax.plot(boundary, [0 for _ in boundary], color="black")


    if draw_reconstruction:

        remap.set_reconstruction_type_for_cells(mesh)

        def color_based_on_reconstruction(reconstruction_type):
            if reconstruction_type[0] == "THINC":
                return (1, 0, 0, 1) # red
            elif reconstruction_type[0] == "JUMP":
                return (1, 0.5, 0.5, 1) # yellow
            elif reconstruction_type[0] == "P":
                return (0, 0, 1, 1) # blue
            elif reconstruction_type[0] == "P+BJ":
                return (0, 1, 0, 1) # green
            else:
                raise RuntimeError("Missing reconstruction type")



        for quantity, remappable, pos in zip(["density", "energy", "pressure", "velocity"], ["density", "momentum_density", "internal_energy_density", "kinetic_energy_density"], axs.flat):
            lines = []
            colors = []
            y = min(getattr(mesh.last_regular_cell, quantity), getattr(mesh.first_regular_cell, quantity))
            y -= abs(getattr(mesh.last_regular_cell, quantity) - getattr(mesh.first_regular_cell, quantity))/100


            for cell in mesh.regular_cells():
                lines.append([(cell.left_node.x, y), (cell.right_node.x, y)])
                colors.append(color_based_on_reconstruction(cell.reconstruction_type[remappable]))

            line_collection = mc.LineCollection(lines, colors=colors, linewidths=2)
            pos.add_collection(line_collection)

    # plt.show()
    return fig, points_for_drawing


def wave_history(history: DrawHistory):
    fig, ax = plt.subplots()

    for (time, waves) in zip(history.time, history.waves_pos):
        for wave in waves:
            ax.scatter(wave, time, color="r")

    for (i, (time, shocks)) in enumerate(zip(history.time, history.shock_pos)):
        if i % 1 == 0:
            for shock in shocks:
                ax.scatter(shock, time, color="blue")

    # plt.show()
    return fig
