import copy
import json

import classes, setting_loader, initialize, draw, lagrange, debug
import matplotlib
import remap
import warnings
import numpy
import os

# noh ale 100 P1lim oscilations




# ########################## Drawing settings ##############################################################
draw_after_time = 5

draw_initialization = False
draw_every_step_after_target_time = False
draw_waves_at_the_end = False
draw_reconstruction = True


save_results = True
draw_results = False


# ############################# Problem settings #################################################################
# problem = "const"
problem = "sod"
# problem = "noh"
# problem = "piston"
# problem = "so"
# problem = "ua_piston"
# problem = "sedov"
# problem = "sedov_new"
# problem = "lax"
# problem = "woodward-collela"


# strategy = "euler"
# strategy = "lagrange"
strategy = "ale"

mesh_dimensions = "20"
mesh_dimensions = "40"
mesh_dimensions = "80"
mesh_dimensions = "160"

mesh_dimensions = "320"

# mesh_dimensions = "50"
# mesh_dimensions = "99"
# mesh_dimensions = "100"
# mesh_dimensions = "300"

mesh_type = "equidistant"


# remap_settings = "P4"
# remap_settings = "const"
# remap_settings = "P1"
# remap_settings = "P1lim"
# remap_settings = "P4THINC"
remap_settings = "P4JUMP"
# remap_settings = "P4lim"



# ################################# Settings relating to warnings and pycharm ######################################
# Changes drawing to be in separate window
# matplotlib.use('Qt5Agg')


# Should raise exception in case of a NaN (Only for Numpy)
numpy.seterr(invalid='raise')


# warnings.simplefilter('always')
warnings.filterwarnings("ignore", message='Extremely bad integrand*')
####################################################################################

if __name__ == "__main__":

    print("Currently solving the " + problem + " problem with " + strategy + " rezone on a " + mesh_dimensions + " cell " + mesh_type + " mesh")
    settings = setting_loader.load_settings(problem, strategy, mesh_dimensions, mesh_type, remap_settings)


    identifier = problem + "-" + remap_settings + "-" + strategy + "-" + mesh_dimensions + "-" + mesh_type
    folder = "Results/"+identifier
    if save_results:
        path = os.path.join(os.path.dirname(__file__), folder)
        if not os.path.exists(path):
            os.makedirs(path)

    mesh, runtime = initialize.initialize(settings)
    history = classes.DrawHistory()

    initial_mesh = copy.deepcopy(mesh)

    switched_regime = False

    fig, _ = draw.draw_all_quantities(mesh, runtime, draw_ghost_cells=False, draw_waves=True,
                                   time_increment=draw_after_time, draw_shock=True,
                                   draw_reconstruction=draw_reconstruction)

    fig.show()
    # if not draw_initialization:
        # draw.drawing_target_time += draw_after_time

    while runtime.elapsed_time < mesh.params.final_time:
        if draw.drawing_target_time <= runtime.elapsed_time:
            fig = draw.draw_all_quantities(mesh, runtime, draw_ghost_cells=False, draw_waves=True,
                                           time_increment=draw_after_time, draw_shock=True,
                                           draw_reconstruction=draw_reconstruction)
            if draw_results:
                fig.show()
            if save_results:
                fig.savefig(folder+"/"+str(runtime.elapsed_time)+".svg", format='svg', dpi=1200)

        if runtime.elapsed_time >= draw_after_time and not switched_regime and draw_every_step_after_target_time:
            switched_regime = True
            print("Switching draw regime")
            draw_after_time = 0
            draw.drawing_target_time = 0

        try:
            mesh = lagrange.step(mesh, runtime, history)

        except classes.NegativeEnergyError:
            runtime.dt = runtime.dt * runtime.c_dt_decrease

        if runtime.current_timestep % mesh.params.remap_after_steps == 0:
            mesh = remap.rezone_and_remap(mesh)

        if runtime.current_timestep % 50 == 0:
            debug.write_out(mesh, runtime)

    print("Done!")
    debug.write_out(mesh, runtime)
    fig, points_for_drawing = draw.draw_all_quantities(mesh, runtime, draw_ghost_cells=False, draw_waves=True,
                                   time_increment=draw_after_time, draw_shock=True,
                                   draw_reconstruction=draw_reconstruction)
    if draw_results:
        fig.show()
    if save_results:
        fig.savefig(folder + "/" + "final" + ".svg", format='svg', dpi=1200)
    json.dump(points_for_drawing, open(folder + "/" + "raw_output.json", 'w'))
    json.dump(points_for_drawing, open("raws/" + identifier + ".json", 'w'))

    fig.savefig("Results/final/" + identifier + ".svg", format='svg', dpi=1200)

    if draw_waves_at_the_end:
        fig = draw.wave_history(history)
        if draw_results:
            fig.show()
        if save_results:
            fig.savefig(folder + "/" + "waves" + ".svg", format='svg', dpi=1200)
