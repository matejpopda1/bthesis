import warnings

import numpy as np

import mesh
import copy
import initialize
import scipy.integrate

import classes


remapable_keys = ["density", "momentum_density", "internal_energy_density", "kinetic_energy_density"]


# switch_for_remap = "energy_fix"
switch_for_remap = "from_velocity"


def rezone_and_remap(original_mesh: mesh.Mesh):
    if original_mesh.params.rezone_type == "copy":
        return copy.deepcopy(original_mesh)

    new_mesh = mesh.copy_mesh(original_mesh)
    rezone(new_mesh, original_mesh)

    remap(new_mesh, original_mesh)

    return new_mesh


def rezone(new_mesh: mesh.Mesh, old_mesh: mesh.Mesh):
    if new_mesh.params.rezone_type == "euler":
        initialize.initialize_mesh_geometry(new_mesh)
    elif new_mesh.params.rezone_type == "laplace":
        laplace_smoothing(new_mesh, old_mesh)
    else:
        raise ValueError("Wrong setting for rezone")


def laplace_smoothing(new_mesh: mesh.Mesh, old_mesh: mesh.Mesh):
    for i in range(old_mesh.params.number_of_cells + 1):
        new_mesh.get_node(i).x = (2*old_mesh.get_node(i).x + old_mesh.get_node(i - 1).x + old_mesh.get_node(i + 1).x)/4


def set_reconstruction_type_for_cells(original_mesh: mesh.Mesh):
    default_reconstruction = original_mesh.params.default_reconstruction
    reconstruction_around_shock = original_mesh.params.reconstruction_around_shock
    reconstruction_in_wave = original_mesh.params.reconstruction_in_wave
    reconstruction_around_wave = original_mesh.params.reconstruction_around_wave



    for cell in original_mesh.cells:
        for key in remapable_keys:
            cell.reconstruction_type[key] = default_reconstruction

    for cell in original_mesh.cells:
        for key in remapable_keys:
            if cell.compressed:
                for cell_near_shock in (cell.exclusive_cell_neighbors(np.ceil(default_reconstruction[1]/2)) + [cell]):
                    cell_near_shock.reconstruction_type[key] = reconstruction_around_shock

    for wave in original_mesh.waves:
        cell = wave.get_cell_the_wave_is_in()
        for key in remapable_keys:
            cell.reconstruction_type[key] = reconstruction_in_wave
            for neighbor in cell.exclusive_cell_neighbors(np.ceil(default_reconstruction[1]/2)):
                if neighbor.reconstruction_type[key][0] == "P":
                    neighbor.reconstruction_type[key] = reconstruction_around_wave

    # print(original_mesh.waves[0].get_cell_the_wave_is_in().reconstruction_type)


def reconstruction(old_mesh: mesh.Mesh):
    for key in remapable_keys:
        reconstruct_quantity(old_mesh, key)


def reconstruct_quantity(old_mesh: mesh.Mesh, quantity):

    for i, cell in enumerate(old_mesh.regular_cells()):
        reconstruction_type = cell.reconstruction_type[quantity]

        if reconstruction_type[0] not in ["P", "P+BJ", "THINC", "JUMP"]:
            raise NotImplementedError("Not implemented reconstruction type")

        if reconstruction_type[0][0] == "P":
            if reconstruction_type[1] == 0:
                cell.derivative[quantity] = []
                calculate_conservation_constants(cell, 0)
            elif reconstruction_type[1] == 1:
                cell.derivative[quantity] = [(cell.right_neighbor.get_quantity_remap(quantity) - cell.left_neighbor.get_quantity_remap(quantity))/(cell.right_neighbor.x - cell.left_neighbor.x)]
                calculate_conservation_constants(cell, 1)
            elif reconstruction_type[1] > 1:
                cell.derivative[quantity] = calculate_higher_order_derivatives_in_a_cell(cell, old_mesh, quantity)
            else:
                raise ValueError("Wrong polynomial degree")

    for i, cell in enumerate(old_mesh.regular_cells()):
        reconstruction_type = cell.reconstruction_type[quantity]
        if reconstruction_type[0] == "P+BJ":
            if reconstruction_type[1] == 0:
                warnings.warn("Trying to use P+BJ on a constant reconstruction")
                return
            if reconstruction_type[1] > 1:
                raise NotImplementedError("BJ limiter is only implemented for maximum degree of polynomial of 1 ")
            local_maximum = max(cell.get_quantity_remap(quantity), cell.right_neighbor.get_quantity_remap(quantity), cell.left_neighbor.get_quantity_remap(quantity))
            local_minimum = min(cell.get_quantity_remap(quantity), cell.right_neighbor.get_quantity_remap(quantity), cell.left_neighbor.get_quantity_remap(quantity))
            phi = 1

            def first_order_tailor(x):
                return cell.get_quantity_remap(quantity) + cell.derivative[quantity][0] * (x - cell.x)

            for node in cell.edge_nodes():
                denominator = first_order_tailor(node.x) - cell.get_quantity_remap(quantity)
                if denominator > 0:
                    phi = min(phi, (local_maximum - cell.get_quantity_remap(quantity))/denominator)
                elif denominator < 0:
                    phi = min(phi, (local_minimum - cell.get_quantity_remap(quantity))/denominator)

            cell.derivative[quantity][0] = phi * cell.derivative[quantity][0]

        if reconstruction_type[0] == "THINC":
            cell.theta[quantity] = np.sign(old_mesh.get_cell(i+1).get_quantity_remap(quantity) - old_mesh.get_cell(i-1).get_quantity_remap(quantity))
            cell.beta[quantity] = old_mesh.params.THINC_beta

            def reconstruction_at_k_cell_at_x_linear(k, x):
                return old_mesh.get_cell(k).get_quantity_remap(quantity) + old_mesh.get_cell(k).derivative[quantity][0] * (x - old_mesh.get_cell(k).x)

            value_at_left_boundary = reconstruction_at_k_cell_at_x_linear(i-1, cell.left_node.x)
            value_at_right_boundary = reconstruction_at_k_cell_at_x_linear(i+1, cell.right_node.x)

            cell.a_min[quantity] = min(value_at_left_boundary, value_at_right_boundary)
            cell.a_max[quantity] = max(value_at_left_boundary, value_at_right_boundary)

            a_min = cell.a_min[quantity]
            a_max = cell.a_max[quantity]

            value = cell.get_quantity_remap(quantity)

            if (value > a_max) or (value < a_min):
                cell.reconstruction_type[quantity] = ["P", 0]
            else:

                # these values could be omitted but more readable this way
                beta = cell.beta[quantity]
                theta = cell.theta[quantity]

                # rescaling of value to interval of 0 to 1 (as in the paper)
                normalized_value = (cell.get_quantity_remap(quantity) - a_min)/(a_max - a_min)

                numerator = np.exp((beta/theta) * (1 + theta - 2 * normalized_value))
                denominator = (1 - (np.exp((beta/theta) * (1 - theta - 2 * normalized_value))))
                inside_of_log = numerator/denominator

                # print("")
                # print(numerator, denominator, cell.self_index)

                cell.hyperbolic_conservation_constant[quantity] = 1/(2 * beta) * np.log(inside_of_log)

        elif reconstruction_type[0] == "JUMP":
            def reconstruction_at_k_cell_at_x_linear(k, x):
                return old_mesh.get_cell(k).get_quantity_remap(quantity) + old_mesh.get_cell(k).derivative[quantity][0] * (x - old_mesh.get_cell(k).x)
            #
            # left_val = reconstruction_at_k_cell_at_x_linear(i-1, cell.left_node.x)
            # right_val = reconstruction_at_k_cell_at_x_linear(i+1, cell.right_node.x)

            left_val = cell.left_neighbor.get_quantity_remap(quantity)
            right_val = cell.right_neighbor.get_quantity_remap(quantity)
            left_node = cell.left_node.x
            right_node = cell.right_node.x


            cell.a_min[quantity] = min(left_val, right_val)
            cell.a_max[quantity] = max(left_val, right_val)

            a_min = cell.a_min[quantity]
            a_max = cell.a_max[quantity]

            value = cell.get_quantity_remap(quantity)

            if (value > a_max) or (value < a_min):
                cell.reconstruction_type[quantity] = ["P", 0]
                return



            jump_position = (cell.get_quantity_remap(quantity) * (right_node - left_node) - right_node * right_val + left_val * left_node)/(left_val - right_val)

            cell.left_val[quantity] = left_val
            cell.right_val[quantity] = right_val
            cell.jump_position[quantity] = jump_position


def calculate_higher_order_derivatives_in_a_cell(cell: mesh.Cell, old_mesh: mesh.Mesh, quantity):
    degree_of_reconstruction = cell.reconstruction_type[quantity][1]
    derivatives = [0] * degree_of_reconstruction

    calculate_conservation_constants(cell, degree_of_reconstruction)


    # calculate how many neighbours are needed
    number_of_neighbours = degree_of_reconstruction
    if number_of_neighbours % 2 != 0:
        number_of_neighbours += 1

    # creating the system
    a = np.zeros((number_of_neighbours, degree_of_reconstruction))
    b = np.zeros(number_of_neighbours)
    nn = - 1
    for neighbour_cells_index in range(cell.self_index - number_of_neighbours//2,  cell.self_index + number_of_neighbours//2 + 1):
        if neighbour_cells_index == cell.self_index:
            pass
        else:
            nn += 1
            for slopes in range(1, degree_of_reconstruction + 1):
                def f(x):
                    return (x - cell.x)**slopes - cell.conservation_constants[slopes]
                temp = scipy.integrate.quad(f, old_mesh.get_node(neighbour_cells_index).x, old_mesh.get_node(neighbour_cells_index + 1).x)[0]
                a[nn, slopes - 1] = temp / cell.volume**(slopes - 1) / old_mesh.get_cell(neighbour_cells_index).volume
            b[nn] = old_mesh.get_cell(neighbour_cells_index).get_quantity_remap(quantity) - cell.get_quantity_remap(quantity)

    # calculating pseudoinverse
    psinv = scipy.linalg.pinv(a)

    # calculating result
    result_of_linalg = np.matmul(psinv, b)

    # rescaling
    for i in range(degree_of_reconstruction):
        derivatives[i] = result_of_linalg[i] / cell.volume**i
    return derivatives




def remap(new_mesh, old_mesh):
    set_reconstruction_type_for_cells(old_mesh)
    prepare_quantities(old_mesh)

    reconstruction(old_mesh)

    for cell in new_mesh.regular_cells():
        for quantity in remapable_keys:
            remap_cell_quantity(cell, old_mesh, quantity)

    compute_new_values(new_mesh)


def prepare_quantities(original_mesh):  # includes BC
    for cell in original_mesh.cells:
        cell.density = cell.density
        cell.internal_energy_density = cell.density*cell.energy
        cell.momentum_density = cell.velocity * cell.density
        cell.kinetic_energy_density = cell.internal_energy_density + 0.5 * cell.density * (cell.velocity**2)


def compute_new_values(new_mesh: mesh.Mesh):
    def state_equation_pressure(i):
        return i.density * i.energy * (new_mesh.params.gamma - 1)


    for cell in new_mesh.regular_cells():
        if switch_for_remap == "from_velocity":
            cell.density = cell.mass/cell.volume
            cell.velocity = cell.momentum/cell.mass
            cell.energy = max(classes.Constants.minimum_energy, cell.kinetic_energy/cell.mass - 0.5*cell.velocity**2)
            cell.pressure = state_equation_pressure(cell)


        elif switch_for_remap == "energy_fix":
            cell.density = cell.mass/cell.volume
            cell.velocity = cell.momentum/cell.mass

            ek_rem = cell.kinetic_energy - cell.total_internal_energy
            ek_new = 0.5*cell.mass*cell.velocity**2
            ek_fix = ek_rem - ek_new
            cell.energy = max(classes.Constants.minimum_energy, (cell.total_internal_energy + ek_fix)/cell.mass)

            cell.pressure = state_equation_pressure(cell)

        else:
            raise RuntimeError("wrong setting")





def remap_cell_quantity(cell, old_mesh, quantity):
    total_mass = 0
    colliding_cells = intersecting_cells(cell, old_mesh)


    if len(colliding_cells) == 1:
        total_mass += integrate_cell(colliding_cells[0], cell.left_node.x, cell.right_node.x, quantity)
    elif len(colliding_cells) == 2:
        total_mass += integrate_cell(colliding_cells[0], cell.left_node.x, colliding_cells[0].right_node.x, quantity)
        total_mass += integrate_cell(colliding_cells[-1], colliding_cells[-1].left_node.x, cell.right_node.x, quantity)
    elif len(colliding_cells) > 2:
        for intersecting_cell in colliding_cells[1: -1]:
            total_mass += intersecting_cell.get_quantity_remap(quantity) * intersecting_cell.volume
        total_mass += integrate_cell(colliding_cells[0], cell.left_node.x, colliding_cells[0].right_node.x, quantity)
        total_mass += integrate_cell(colliding_cells[-1], colliding_cells[-1].left_node.x, cell.right_node.x, quantity)

    cell.set_quantity_remap(quantity, total_mass)



def integrate_cell(cell: mesh.Cell, a, b, quantity):
    def helper(x):
        return evaluate_reconstruction_at_x(cell, quantity, x)

    result = scipy.integrate.quad(helper, a, b)

    return result[0]


def evaluate_reconstruction_at_x(cell: mesh.Cell, quantity, x):
    if cell.reconstruction_type[quantity][0][0] == "P":
        if cell.reconstruction_type[quantity][1]==0:
            return cell.get_quantity_remap(quantity)
        result = 0
        h = x - cell.x
        for i, derivative in enumerate([cell.get_quantity_remap(quantity)] + cell.derivative[quantity]):
            result += (pow(h, i) - cell.conservation_constants[i]) * derivative
        return result
    elif cell.reconstruction_type[quantity][0] == "THINC":
        beta = cell.beta[quantity]
        theta = cell.theta[quantity]
        hyperbolic_conservation_constant = cell.hyperbolic_conservation_constant[quantity]
        a_min = cell.a_min[quantity]
        a_max = cell.a_max[quantity]

        x_left_boundary = cell.left_node.x
        x_right_boundary = cell.right_node.x

        argument_of_tanh = beta * (((x - x_left_boundary) / (x_right_boundary - x_left_boundary)) - hyperbolic_conservation_constant)
        result = a_min
        result += (a_max - a_min) / 2 * (1 + theta * np.tanh(argument_of_tanh))
        return result
    elif cell.reconstruction_type[quantity][0] == "JUMP":
        # print(cell.jump_position)
        jump_position = cell.jump_position[quantity]
        left_val = cell.left_val[quantity]
        right_val = cell.right_val[quantity]

        if x < jump_position:
            return left_val
        else:
            return right_val
    else:
        raise RuntimeError("Unknown reconstruction type")


# def intersecting_cells(cell, old_mesh: mesh.Mesh):
#     result = []
#     for i in old_mesh.cells:
#         if cell.left_node.x < i.x < cell.right_node.x or i.left_node.x < cell.left_node.x < i.right_node.x or i.left_node.x < cell.right_node.x < i.right_node.x:
#             result.append(i)
#     return result

def intersecting_cells(cell, old_mesh: mesh.Mesh):  # Faster implementation
    result = []

    # cells_to_iterate_over = [old_mesh.get_cell(cell.self_index)]
    # i = 1
    #
    # while not (cell.left_node.x > cells_to_iterate_over[0].left_node.x and cell.right_node.x < cells_to_iterate_over[-1].right_node.x):
    #     cells_to_iterate_over = old_mesh.get_cell(cell.self_index).exclusive_cell_neighbors(i)
    #     i += 1

    # TODO: This is way too slow, also we just ignore ghost node which idk
    for i in old_mesh.regular_cells():
        if cell.left_node.x < i.x < cell.right_node.x or i.left_node.x < cell.left_node.x < i.right_node.x or i.left_node.x < cell.right_node.x < i.right_node.x:
            result.append(i)
    return result


def calculate_conservation_constants(cell: mesh.Cell, degree):
    cell.conservation_constants = [0] * (degree + 1)

    for i in range(2, degree + 1):  # calculates the conservation constants, ignoring the first 2 since its analytically 0
        def f(x):  # could be rewritten with lambda function
            return pow((x - cell.x), i)

        cell.conservation_constants[i] = scipy.integrate.quad(f, cell.left_node.x, cell.right_node.x)[0] / cell.volume
