import matplotlib.pyplot as plt
import numpy as np


x = np.arange(0, 4, 0.1)
y = (x**2+2*x)/(x**2+x+2)
z = np.minimum(x, 1)

plt.plot(x, y, c="black", label="Průběh Venkatakrishnanova limiteru")

plt.plot(x, z, c="blue", label="Průběh BJ limiteru")
plt.legend()

plt.xlabel("x")

plt.savefig("limiters.svg")
