import matplotlib.pyplot as plt
import numpy as np

def create_nodes_for_cyclic_remapping(n, n_max, i_max, starting_point=0, ending_point=1):
    result = []
    t_n = n/n_max

    def alpha(t):
        return np.sin(4*np.pi*t)/2

    def x(eta, t):
        return (1 - alpha(t))*eta + alpha(t)*eta**3

    for i in range(i_max - 1):
        eta_i = i / (i_max - 1)
        result.append(x(eta_i, t_n) * (ending_point - starting_point) + starting_point)
    result.append(ending_point)  # must be on a separate line, otherwise numerical inaccuracy messes up with the result

    return result




fig, ax = plt.subplots()

for t in np.linspace(0, 1, 500):
    plt.scatter(create_nodes_for_cyclic_remapping(t, 1, 10), [t]*10, color="blue")


ax.set_xlabel("x")
ax.set_ylabel("t")

plt.savefig("meshmovement.svg")
plt.show()


